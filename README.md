# MtAw2_ReferenceSheets

Reference sheets for Mage the Awakening 2nd Edition

Sheets are kept as close to RAW as possible, but may contain misinterpretations where abbreviated, e.g. mage sight rules.

Pdf's can be found as artifacts of most recent build.

Build is currently run at every commit.

## Local docker compile:

Compiling all tex documents locally with docker can be done by executing:

- `cd PROJECT-DIRECTORY`
- `docker run -ti --rm -v miktex:/miktex/.miktex -v "$(PWD):/miktex/work" miktex/miktex bash compile.sh`

This will create a Bin folder, containing the same subdirectories as in Tex, with all the corresponding pdfs.

## To do: 
### ~~Add Arcana Sheets~~
 - ~~Death~~
 - ~~Matter~~
 - ~~Life~~
 - ~~Spirit~~
 - ~~Mind~~
 - ~~Space~~
 - ~~Time~~
 - ~~Fate~~
 - ~~Forces~~
 - ~~Prime~~

### Add Miscellaneous Sheets
 - ~~Spellcasting~~
 - ~~Mage Sight~~
 - Conditions
 - Legacy rules
 - Creative Thaumaturgy
 - Arcanum Attainments

### Other
 - Ensure all spells are updated with errata changes

### Improve
 - Remove previous artifacts/builds when building
 - Output build results into a more accessible storage
 - Build patcher/modder for handling configurations of original reference sheets. E.g. Campaign specific changes.
 - Improve file structure, for easier assembling of combined documents
 - Better bookmarks for Spells. Could use some more levels, to avoid very long lists
 - More stuff to come.
