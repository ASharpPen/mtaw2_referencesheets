\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Forces}

\section{Forces}
\textbf{Purview:} Electricity, gravity, radiation, sound, light, heat, fire, weather, movement. \\
The gross Arcanum of the Aether governs the mightiest energies of the Fallen World. Countless legends of wizards conjuring lightning to smite their foes, dancing among pillars of unnatural flame, flying, and directing storms against their foes speak to the presence of raw power Forces represents. With it a mage can alter and control light, sound, fire, and electricity - even gravity, radiation, and weather patterns. Forces is rarely subtle, but clever wizards find ways to use it so: hearing a sound from across a room, deadening the noise spellcasting makes, or seeing great distances. Skilled practitioners of Forces can also unleash tornadoes, earthquakes, and devastating blasts of fire when subtlety gives way to quick anger.

\subsection{Playing With Fire}
Many Forces effects are just as harmful to the mage herself as they are to any potential victims. If the mage does not Reach to cast them at sensory range, she could just as easily electrocute or incinerate herself in the process. Some effects are beneficial enough that the mage need not worry about harming herself, such as warming a cold area or lighting a dark room, but proper caution is advised when conjuring fire, explosions, electricity, and other such destructive forces.

\spell{Influence Electricity}
{Compelling}
{Duration}
{}
{}
{The mage can operate or shut down electrical devices with magic. With this spell she can only cause existing devices to work as they normally would when powered on, or when the power is shut off. For example, she could "hotwire" a car without actually needing to touch any wires, turn lights off and on, and
cause industrial machinery to power up or turn off. This spell does not give her further control over these devices, but does allow her to engage or shut down devices that might otherwise require passwords or electronic keys.
}

\spell{Influence Fire}
{Compelling}
{Duration}
{}
{}
{Legends of mages controlling fire begin with this spell, which allows a mage to guide the path of existing flames. This lets her cause flames to arc or stretch, command them to burn along a particular path (or prevent them from another), or even form particular fiery shapes. At this level the mage cannot increase the flames in size or intensity, though she could direct them into a source of fuel.
\reach[1]{By efficient use of fuel and direction, the mage can increase or  decrease the size of a fire by one level (see Transform Energy, below).}
}

\spell{Kinetic Efficiency}
{Compelling}
{Duration}
{}
{}
{With a simple spell, the mage can "nudge" kinetic forces, enhancing a subject's motion. This spell allows the subject to run a little faster, jump a little further, or lift a little more, not by altering forces but by maximizing the subject's kinetic energy use. This has the following benefits:
\begin{smallitemize}
\item The subject gains a bonus on rolls to resist fatigue equal to the spells potency. Actions are less strenuous when moving so efficiently.
\item Add the spells potency to the total distance (in yards) covered on a jump, to the subject's swimming and running Speed, and to any climbing rolls.
\end{smallitemize}
}

\spell{Influence Heat}
{Compelling}
{Duration}
{}
{}
{Initiates can guide the direction of existing forces. With this spell, the mage can control the flow of heat in the area. While she cannot increase or create heat, the mage can direct heat from a radiator across the room to her, or pull any ambient warmth shed by car engines, human bodies, or environmental sources. This can keep her warm in cold weather or cool in hot weather, preventing heat- or cold-related damage and Conditions caused by Extreme
Environments up to Level 2 (see Extreme Environments, p. 224).
\reach[1]{The mage can safely control the flow of heat enough to keep safe in environments up to Level 3.}
\reach[2]{At this level, the mage can protect herself and others in the spell's area from hot or cold environments up to Level 4.}
}

\spell{Nightvision}
{Unveiling}
{Duration}
{}
{}
{Despite its name, the Nightvision spell enhances ambient light, fine-tunes the mage's sense for vibrations and thermal changes, and grants her the power to see into the infrared and ultraviolet spectra. She becomes able to intuitively feel as well as see forms of electromagnetic radiation, sound, and kinetic
energies, allowing her to navigate without penalty in complete darkness. She can still see and make out details, even in the dark, although colors are somewhat muted. This spell has the side effect of making the caster much more vulnerable to light; while in effect, she suffers no penalties from dim or even no lighting, but suffers penalties from bright lights as she normally would from darkness. Bright lights and extremely loud sounds can disorient or even inflict the Blind Condition on her for the spell's Duration.
\reach[1]{The spell automatically compensates for and allows the mage to ignore the deleterious effects of sudden extreme stimulus.}
}

\spell{Receiver}
{Unveiling}
{Duration}
{}
{}
{Casting this spell allows the mage to hear infrasound and ultrasound frequencies beyond what human ears can normally perceive. While active, she can hear sounds outside the normal frequency, from high-frequency (dog whistles, sonar) to low-frequency (the distant rumble of diesel engines, industrial sounds normally lost to humans in the noise). Apply the spell's
Potency as a dice bonus to relevant dice pools, such as rolls to avoid ambush.
}

\spell{Tune In}
{Knowing}
{Duration}
{}
{}
{A mage with this spell can listen to free-floating data transmissions, such as those broadcast by radios, cell phones, wireless modems, and more. The magic translates this electromagnetic noise into something she can understand, although it preserves the original transmission language. With this spell, the mage needs no receiver to listen or even see signals. Transmitting cables glow before her eyes with streams of data, while she might see a shimmer or even fleeting glimpses of images in the air. Satellite internet and TV programming, closed walkie-talkies, CB broadcasts, and radio transmissions all become open to her senses, as well as wireless communications.
}

\spell{Control Electricity}
{Ruling}
{Duration}
{}
{}
{The mage may alter the flow of electricity as well as diminish its current. She cannot increase the current without some device capable of generating it, since she cannot create electricity from nothing. For example, the mage can direct the electricity in a building to single or multiple outlets, cut the power, or divide power going to one outlet to many other sources. This requires a method of conduction, like existing wiring or metal. She can also cause existing electrical currents to arc (such as striking a target near a wall outlet), or redirect it away from a particular device. Causing a short or using the electrical current to attack a target usually burns out the breakers or shorts
out a device afterward, unless it's made to withstand the stresses of power fluctuations. Damage caused by this spell uses the electrical damage rules on p. 224. By directing the flow away from a subject, she subtracts the spell's Potency from the damage of an electrical source. Each level of Potency in the spell allows the mage to control one line of power. If she diverts it somewhere else, the Storyteller determines what occurs — a socket or device could overload, or if the mage is careful she can avoid damaging components
and simply change the course of power. If instead she wants to diminish the power, it drops by one level for each point of Potency; a Potency of 4 reduces a subway line's current to that of an ordinary wall socket, for example, or Potency 5 cuts it completely. (See the table below under Transform Energy.)
}


\spell{Control Fire}
{Ruling}
{Duration}
{}
{}
{The mage can exert control over a fire, fueling it to increase its size and intensity or depriving it of fuel to snuff it out. She can only control existing flames at this level, but can change a small campfire into a roaring inferno or bring even an out-of control fire down to manageable levels. For each level of Potency, the mage chooses one of the following effects (see Transform Energy, below):
\begin{smallitemize}
\item Increase or decrease the heat of the fire by one level.
\item Increase or decrease the size of the fire by one level.
\end{smallitemize}
If the fire's heat or size are reduced to less than one, it is extinguished. Unless extinguished, once the spell's Duration ends a diminished fire will eventually spread back out along available fuel.
}

\spell{Control Gravity}
{Ruling}
{Duration}
{}
{}
{The mage can redirect the force of gravity in an area. She can alter the direction of its pull, causing affected objects to "fall" upwards or horizontally. She can't do more than change its direction at this level, but she can make it nearly impossible to approach a specific object or area without some means of overcoming gravity, like flight or climbing gear. Anyone and anything affected by the spell that is not secured "falls" in the direction chosen by the caster. Victims may suffer damage if they collide with objects. Someone trapped in an area where gravity propels him upward might be stuck falling to the edge of the spell's radius, then back down again as normal gravity takes over, only to fall upward again as he enters the spell's area. A person or creature capable of action can make a roll to escape, at the Storyteller's discretion, by grabbing onto a nearby object or otherwise finding the means to control her position.
}

\spell{Control Heat}
{Ruling}
{Duration}
{}
{}
{The mage can now increase or decrease the temperature of an area. Each level of Potency allows a change of 1 level of Extreme Environment to produce heat or cold, counting a temperate room temperature as "zero." For example, with Potency 3, a mage could transform a Level 1 Extreme Environment based on cold into a Level 2 Environment based on heat.
}

\spell{Control Light}
{Ruling}
{Duration}
{}
{}
{The mage can dim or intensify existing light the spell's area of effect, whether from an artificial or natural source. This can cause a 40-watt bulb to shine as brightly as a floodlight or make the sunlight on an overcast day like that of a clear summer morning. The magic modifies the light emitted by the source, and not the source or the emission itself, so this won't cause a bulb to burn itself out or increase the heat of sunlight without other spells. Each level of Potency in the spell doubles or halves the light's candescence. The spell allows the mage to focus or disperse light, and even alter its wavelength on the spectrum. She could turn a torch into a blacklight, focus a lamp's rays into a laser, split its lights into a rainbow spectrum as though viewed through a prism, or cause a refraction effect like looking upon something in shallow
water. These effects cause the Poor Light Tilt in the affected area.
\reach[1]{The spell can create a mirroring effect or complete black-out by turning all light in the area back on itself or away from onlookers. The spell imposes the Blinded Tilt in the affected area, or provides substantial cover, although as the effect is only visual the cover has no Durability.}
}

\spell{Control Sound}
{Ruling}
{Duration}
{}
{}
{This spell allows a mage to amplify or weaken the volume of sound in the spell's area of effect. She can make a loudspeaker into a thunderous blast or a barely audible squeak. Each level of Potency doubles or halves the volume of sound in the targeted area, creating a zone of altered sound. For example, casting this spell on a podium at the front of the room affects the sounds
of anyone standing at the podium. The mage can also influence the direction of existing sounds. She can focus sound waves from across the room to hear a whispered conversation, ensure her own voice does not reach anyone but her intended target, or cause noises to emanate from nearby locations instead of their original sources. The Scale factor determines the area she can affect.
Loud enough sounds can cause the Deafened Tilt in combat. By directing her sound away from a subject that might possibly notice her, the mage inflicts a penalty equal to her Arcanum dots to the subject's Perception rolls to hear her approach.
\begin{smallitemize}
\item Focusing sound waves to a specific point means that anyone outside of the chosen target (as determined by the Size of Target table) cannot hear the chosen sounds.
\item Listening to sounds across an area uses the Area of Effect Scale factor, determining the distance at which the mage can listen in on something. Doing so does not rob the source of its sound. To do that requires focusing the sound completely away from the source (above). This spell can also be used to alter the tone of a given sound, including modifying a target's voice to sound like another. Emulating a specific voice might require an Expression + Subterfuge roll to get it right.
\end{smallitemize}\fixEmptyNewline{}
\reach[1]{The mage can create an echoing effect by "nudging" sound waves into nearby obstacles. This imposes a penalty to Stealth rolls in the affected area equal to the spell's Potency, to a maximum of –5.}
\reach[1]{The caster gains a bonus to hearing-based Perception rolls within the affected area equal to the Potency of the spell, to a maximum of +5.}
}

\spell{Control Weather}
{Ruling}
{Duration}
{}
{}
{The mage can control existing weather patterns. She can force a light shower to become a thunderstorm, summon up fog on a clear morning, make a warm day into an unbearably hot one or conjure a cooling breeze. Drastic changes from existing weather require Reach, as noted below. The weather begins to change immediately upon casting the spell, with new systems taking shape within minutes. This spell allows the mage to change or create weather-based Extreme Environments up to Level 4, as well as cause a wide variety of environmental tilts. Potency determines the maximum amount by which an Extreme Environment can change, up to a maximum of Level 4
(and a minimum of level 0). Listed below is an example set of Conditions possible for weather patterns, but it is by no means exhaustive. Note that without further spells, the mage is not immune to weather Conditions she creates. In combat, Tilts are used in place of Conditions.
\begin{smallitemize}
\item Blizzard
\item Extreme Cold
\item Extreme Heat
\item Heavy Rain
\item Heavy Winds
\end{smallitemize}\fixEmptyNewline{}
\reach[1]{The weather changes more gradually, over the next few hours, giving the caster time to prepare or making the weather seem more natural.}
\reach[2]{Required for more drastic changes. Examples:
\begin{smallitemize}
\item Creating thick fog out of a sunny morning
\item Making a hot summer day turn to freezing rain
\item Clearing a blizzard into cool but clear evening
\end{smallitemize}}
}

\spell{Environmental Shield}
{Shielding}
{Duration}
{}
{}
{The mage can shield herself against harmful environmental conditions. This spell provides complete resistance to any Conditions or Tilts caused by environments, up to an Extreme Environment level of the spell's Potency. The spell only protects against indirect damage, like heat and cold and minor hazards like hail. The mage can still drown or be crushed by crashing waves. While the spell wouldn't protect her against lightning if something forced it to strike her, she wouldn't naturally attract the bolt. The spell requires a Clash of Wills to work against magical weather effects.
}

\spell{Invisibility}
{Veiling}
{Duration}
{}
{}
{This spell can render its subject completely invisible, masking it from all forms of light. Even cameras cannot detect the object, no matter what type of filter or lenses they use. This spell does not mask the sounds an object makes, although when Combined with "Control Sound" (see above), the target can be made invisible and soundless.
}

\spell{Kinetic Blow}
{Ruling}
{Duration}
{}
{}
{The mage focuses the kinetic force of bludgeoning attacks to such a pinpoint that they cause damage like piercing weapons. This only works on the subjects unarmed attacks and not any held weapons, though body-hugging objects like gloves and shoes benefit from the spell's effect. Unarmed attacks (including those made in grapples) gain a weapon bonus equal to Potency, to a maximum of 5. Unarmed attacks become Lethal.
\reach[1]{The spell applies the Knocked Down Tilt.}
\reach[1]{The spell applies the Stunned Tilt.}
\reach[1]{This spell affects held weapons.}
\reach[2]{The spell affects thrown weapons. Alternately, the spell affects firearms, granting them greater penetrative ability. Bullets gain Armor Piercing equal to the spell's Potency.}
}

\spell{Transmission}
{Ruling}
{Duration}
{}
{}
{The mage can hijack existing signals and change the transmitted data or its destination. She can shorten or lengthen the transmission, but cannot change the type of signal, such as turning a wifi broadcast into a radio signal. At this level, she must still work with a signal already present. Mimicking specific sounds or information requires a Skill roll or access to the data to be transmitted.
\reach[1]{The signal sent can be "encrypted" so that only certain actions, like specific keystrokes or frequencies, can receive them properly. Otherwise, they simply turn up as unintelligible "noise."
(Errata: "can change the frequency").}
}

\spell{Zoom In}
{Ruling}
{Duration}
{}
{}
{The mage focuses light entering her subject's senses, greatly magnifying vision. Without an Unveiling spell like "Nightvision" (p.141), the spell can only affect visible wavelengths. For example, a mage using this spell on herself could look closely at a bird circling high above, or zoom in to great detail to examine a layer of dust on an object, but she couldn't see things that would only appear under a blacklight. If a character magnifies vision to focus on small-scale occurrences, the Storyteller may call for Intelligence + Science rolls to make sense of what she's seeing. Every level of Potency doubles the distance the mage can see clearly before suffering penalties, although atmospheric conditions can still cloud her view. Add Potency to dice rolls to notice small details.
\reach[1]{The subject can see clearly out to a distance of 1 mile per dot of Forces.}
\reach[1]{The subject can clearly discern dust-sized particles.}
\reach[1]{The subject no longer suffers penalties from atmospheric conditions.}
\reach[2]{The subject can see microscopic particles, even the molecular bonds between objects.}
}

\spell{Call Lightning}
{Weaving}
{Potency}
{}
{}
{With a gesture the mage can conjure lightning down from a stormy sky to strike her foes. She must use this spell with an existing storm or one she sets into motion with "Control Weather" (see p. 143), since she cannot create lightning from nothing at this level. While the lightning bolt itself is all but unavoidable, the crackling build-up of energy gives a target warning. The bolt can only strike a target it could actually reach, so the target must be exposed in some way. With multiple subjects, the lightning forks, striking each simultaneously. It does damage according to the Electricity rules on p. 224.
}

\spell{Gravitic Supremacy}
{Perfraying}
{Duration}
{}
{}
{The mage may increase or decrease gravity. If increasing it, each level of Potency subtracts 3 Speed from all subjects, as well as penalizing jumping rolls, subtracting a distance equal to Potency from success rolled. If Potency exceeds the Strength of an animal caught in the area, the subject suffers –1 to all Physical dice pools for each point of difference. Flying creatures must
succeed on a Strength + Athletics roll each turn or plummet downward at a Speed equal to the Potency. Nullifying gravity increases the Speed of anyone within the area of effect by the spell's Potency. Increase jumping distance per success rolled by the spell's Potency. Additionally, the mage can cause objects to fall in any direction she chooses when she creates the spell,
}

\spell{Telekinesis}
{Weaving}
{Duration}
{}
{}
{The mage can conjure telekinetic force to lift or manipulate an object remotely. Apply the spell's Potency to either the force's Strength (raw lifting/pushing power) or its Dexterity (fine manipulation) or Speed. The other two scores defaults to a score of 1. This requires concentration as an instant action each turn; if the mage fails to concentrate on moving the force, it simply hangs suspended, holding any objects it held before but no longer pushing or pulling (or manipulating objects, if used for that). The mage may then resume directing the telekinetic force until the spell's Duration expires.
\reach[1]{Can split some of the Potency to another of the three stats. Reach may be taken twice.}
}

\spell{Telekinetic Strike}
{Weaving}
{Potency}
{}
{}
{The mage manipulates kinetic forces to crush subjects or form a "ball" of highly pressurized air and kinetic energy that she can hurl at foes. The spell inflicts bashing damage equal to its Potency.
\reach[1]{The spell inflicts the Knocked Down or Stunned Tilt.}
}

\spell{Turn Momentum}
{Weaving}
{Potency}
{}
{}
{This spell allows the mage to redirect a target's momentum. Usually this forms a shield against projectiles, but it can be used on larger objects, as well. When a mage could use her Defense against an object, she may use this spell instead to redirect it as an instant action. If cast with a prolonged Duration, the mage may take a Dodge action each turn and use this spell instead of receiving the normal benefits for Dodging. The spell allows the mage to turn a number of moving objects up to Potency. She does not have fine control over where each object is deflected, and the spell cannot make objects reverse
direction entirely, only veer off target. The maximum Size of a redirected object is determined by the spell's Scale factor. Because the spell acts upon an object's momentum and not the object itself, the weight and speed are irrelevant to this spell's effect; if the mage can use the spell, the magic will thwart any object within its parameters. The redirected object maintains all
its original momentum. By default, the Storyteller determines a random direction for the object to travel when redirected.
\reach[1]{The mage can use Turn Momentum's effect as a reflexive action instead.}
\reach[1]{The mage may control where the object is redirected to, as long as it is within 90 degrees of the original arc in any direction.}
\reach[2]{The object's momentum can be fully reversed. Thrown and Ranged weapons strike their users.}
\conjunction{Add Time}{1}{With Time 1 and a Reach, the mage can turn objects too fast for her to gain Defense against them.}
}

\spell{Velocity Control}
{Perfraying}
{Potency}
{}
{}
{The mage can greatly increase or decrease an object's velocity. Its speed doubles or halves for each level of the spell's Potency. For example, a car traveling 50 MPH is increased to 200 MPH with Potency 2, or an incredible 800 MPH with Potency 4. Likewise, if the mage reduces its speed, the same car would slow to about 13 MPH at Potency 2, or about 4 MPH with Potency
4. The mage must be able to affect the target's entire Size to affect it with this spell; she cannot target only the front tire of an 18-wheeler with its trailer (Size 30) and bring it to a stop. The velocity change affects damage based on collisions. It also adds or subtracts one point of damage from projectile attacks and can reduce them to 0. The spell cannot reduce a moving object's speed to 0 (that would require an Unmaking spell).
}

\spell{Electromagnetic Pulse}
{Unraveling}
{Potency}
{}
{}
{The mage creates a pulse of destructive electromagnetic force to unravel electricity present in the spell's subject. The spell is capable of snuffing out mundane electrical devices, although some military-grade devices are shielded, requiring Potency equal to their level of hardening. Shorting out magical devices requires a Clash of Wills. If used on a person, acts as a direct attack spell.
}

\spell{Levitation}
{Patterning}
{Duration}
{}
{Stamina}
{The subject floats through the air using telekinetic force. Its air speed equals the spell's Potency. Subjects may use their Defense against attacks, if applicable. Unwilling subjects Withstand the spell with Stamina. The mage may direct the subject's levitation each turn as an instant action. If she fails to do so, the object simply remains afloat in midair, coming to a stop wherever it was when she stopped moving it.
\reach[1]{The subject retains momentum from turn to turn, floating slowly in whatever direction the mage initially directed it.}
\reach[1]{The subject flies fluid and free. She instinctively creates the telekinetic force as she maneuvers in midair, granting her incredible speed and maneuverability. The subject gains an air Speed equal to the mage's Gnosis + the spell's Potency. While airborne she can make a Dexterity + Athletics roll to avoid obstacles, gains her normal Defense against attacks, and can fly without exhausting herself as she would by running.}
}

\spell{Rend Friction}
{Patterning}
{Potency}
{}
{}
{The mage alters the level of friction upon a target. She can increase it to the point where simple air friction shears the target to pieces, or lower it so much that an object can continue moving almost indefinitely. If the spell increases friction, every 3 yards that the target moves in a given turn (round down) causes it one point of lethal damage (max damage equal to Potency). The target must be exposed to open air, and non-magical armor is only half as effective. Conversely, if the mage decreases friction, an object doubles the length it moves before slowing down per point of Potency, even after it has stopped accelerating or maintaining its speed. This also doubles weapon ranges in the same way. Vehicles suffer a penalty to their Handling equal to the spell's Potency, as the vehicle fails to stop or react in a way the operator can fully control.
}

\spell{Thunderbolt}
{Patterning}
{Potency}
{}
{}
{Like a god of old, the mage gathers ambient energies to strike her subject. This spell deals lethal damage equal to its Potency.
\reach[1]{The spell deals aggravated damage. Cost: 1 Mana.}
}

\spell{Transform Energy}
{Patterning}
{Duration}
{}
{}
{All energy shares sympathy, born perhaps from the same cosmic source in the same instant. An Adept of Forces can use that sympathy to transform one energy type into another. The table below serves as a rough equivalency chart for different energy types. She can change a room full of light into heat, at
once turning it into a pitch-black oven. She might also change the thunderous roar of a waterfall into electricity, far more efficient than any hydroelectric dam. The spell may affect energy of a level equal to Potency.
\reach[1]{The mage may decrease the level of the energy transformed by one. This Reach may be applied multiple times.}
\reach[1]{The mage may split a source of energy into two different kinds or leave part of the original source behind — for example, turning an inferno into light equivalent to daylight and sound equivalent to a scream.}
\reach[1]{For one Mana, the mage may increase the level of the energy transformed by one.}

\begin{table}[!ht]
\textbf{Transform Energy}\\
\setlength\tabcolsep{1.5pt} 
\tiny
\begin{tabular}{l|l|l|l|l|ll}
	\textbf{Level} & \textbf{Light} & \textbf{Sound} & \textbf{Heat (Celsius)} & \textbf{Electricity} & \textbf{Fire} \\
	1 & Flashlight & Casual talk & Room temp. (22) & Car battery & Lighter \\
	2 & Sunlight & Scream & Body temp. (37) & Wall socket & Torch \\
	3 & Car headlight & Electric guitar & Kills bacteria (71) & Electric fence & Bonfire \\
	4 & Floodlight & Gunshot & Boiling Water (100) & Junction box & Inferno \\
	5 & Stadium light & Industrial & Books burn (223) & Main line & Forest fire
\end{tabular}
\end{table}
}

\spell{Adverse Weather}
{Making}
{Duration}
{}
{}
{The mage summons a major weather system as severe as a tornado, tsunami, monsoon, or hurricane. The weather effects take hold in minutes and dissipate immediately once the spell expires. This allows the mage to create Extreme Environments of nearly any kind up to Level 4, as per Change Weather (see above) but without the limitations. She does not have to call up disasters; she can make a rainstorm appear in a cloudless blue sky if she desires.
\reach[1]{The mage can create weather drastically different from the local conditions, such as a monsoon during the savannah's dry season.}
}

\spell{Create Energy}
{Making}
{Duration}
{}
{}
{The mage creates energy from nothing. The amount of energy fills a volume equal to the area targeted by the spell. She can create light (including sunlight), fire, radiation, sound, and electricity. Use the chart under Transform Energy above as an example of the levels she can create within the affected area.
For fire, assume the heat is +1 for Potency 1–2, +2 for Potency 3–4 and +3 for Potency 5+. After creating the energy, she can modify it with Control spells. Creating radiation also creates an Extreme Environment hazardous to living beings.
(Errata: "Fills the subject or Scale factor.")
}

\spell{Eradicate Energy}
{Unmaking}
{Potency}
{}
{}
{Rather than create energy, the mage snuffs out energies in the same volume as she could create them (see the chart "Transform Energy," above). The destruction is spectacular, explosively scattering the affected energies into particles. If used on a creature, this spell is instantly fatal but Withstood by Stamina.
(Errata: "Removes energy from a subject or Scale factor.")
}

\spell{Earthquake}
{Making}
{Potency}
{}
{}
{The mage unleashes an earthquake to crack the ground. This spell inflicts damage equal to its Potency to all structures within the affected area. Most modern buildings are built to endure earthquakes well and subtract their Durability from the damage as normal. Smaller or flimsier structures do not apply Durability to the damage at all. Living beings may make a Dexterity +
Athletics roll to maintain their balance as the ground pitches and heaves beneath them. Failure means the character suffers bashing damage as she falls to the ground and is thrown about wildly, unless the fall sends her tumbling down stairs or over a ledge. Collapsing buildings can cause much more catastrophic damage or leave victims trapped under tons of debris.
}

\end{document}