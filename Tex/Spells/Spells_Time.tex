\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Time}

\section{Time}
\textbf{Purview:} Prophecy, change, postcognition, time travel, time contraction and dilation.\\
Time and Fate rule Arcadia; Time is the gross Arcanum of the pair, governing the progression of events through the ever-advancing present in a way perpendicular to Fate's subtlety. Some Acanthus liken Time to a loosed arrow, and Fate to the archer's aim. Time governs the history and diverse potential futures of the Fallen World. All things must pass. Legends rise and fall. Even gods die and fade into obscurity. Nothing is eternal, save perhaps for time itself.

\subsection{Spinning the Thread of Time}
Sleeper philosophers and scientists debate and make inferences about the nature of Time, discussing theoretical problems and paradoxes, measuring the distortion in the light of distant stars, but always knowing that they are locked into Time's endless march. Mages who study the Time Arcanum have practical experience to marry to Sleeping theory, and quickly discover that while some thinkers have approached something like the truth, thought experiments don't fully prepare them for the reality of temporal magic. Sleepers often describe time as a river, but traditional Acanthus instead describe it as a spinning wheel, gathering strands of unspun material and binding them into a tight thread.

\subsection{The Future is Unwritten}
The unspun material in the metaphor, the future is protean and ever-changing. Using Time spells to access the future only reveals the most likely future at the time the spell is cast, and foreknowledge can and often does then immediately change the outcome. Advanced Time spells rewrite the future like any other Pattern, dictating how immediate events will go or constructing theoretical futures mages then examine and let dissipate into the shifting flow of probability.

\subsection{The Past can be Rewritten}
In contrast, the past is like the spun thread - set and decided, unless magic alters it. Sleepers theorize about temporal paradoxes; if someone goes back in time to avert a disaster and succeeds, surely they would never then have to travel back, so would never have changed events? Magic defies causality; a mage who travels back in time can alter the cause of his own trip and more, the magic accounting for any contradiction caused. When an object or person is in the past, the distortion is visible to onlookers using Active Mage Sight with Time, and as she changes history everything she alters also picks up a telltale temporal aura. When a time traveler returns to the present, any changes he made to the timeline "set," becoming Lasting, and the distortions vanish. Dying while in the past "sets" any changes made up to that point. Travelers are insulated against the alterations of history; a mage who prevents her own birth returns to a world that does not know her, but does not vanish from existence. While still in the past, if the spell that projected the traveler backwards is dispelled, he returns to the present but any changes he made to history are reversed.

\subsection{The Present Moves On}
The Fallen World has a present, the point at which the future becomes the past, which constantly advances. Mages who travel back in time and look forward find that the intervening span is still set, spun from probability to realized future history. While it may be their subjective future, it's in the Fallen World's past. From their perspective, the present is frozen at the moment they left - if they "catch up" to it, anything they changed becomes real, and the normal flow of time resumes. Spells to travel into the future jump the subject out of time until the destination becomes the present.

\subsection{Temporal Sympathy}
The "spun" time of the past is both more certain and useful to examine with magic but harder to affect, as a mage must contend with the weight of all the intervening history. Much as two subjects may have sympathetic links crossing Space marking how magically related they are, a subject has temporal sympathy with its own past selves, which influences magic used with a past version of an object, place, or thing as its subject. The more a subject has changed in the intervening time, the harder it is for a mage to look back at it but the easier it is to affect that sympathy itself. Just as Space magic that alters sympathetic links has consequences for the people or objects concerned, manipulating temporal sympathy can provoke subtle effects; increase a woman's temporal sympathy with her youth and she'll become highly nostalgic, perhaps trying to recapture it. Destroy a building's temporal sympathy with its past and people will forget its history. Magic that affects the "unspun" time of the future is unaffected by temporal sympathy; the shifting potential timelines have no "substance" for magic to contend with.

\newpage
\subsection{Temporal Sympathy}
\textbf{Temporal Sympathy:} Connected\\
\textbf{Withstand (Connection):} 5*\\
\textbf{Withstand (Temporal Sympathy):} 0\\
\textbf{Description:} The two subjects are metaphysically one e.g. a mage and her familiar or soul stone. The connection is unassailable without Unmaking magic and casting using the connection is not Withstood.\vspace{\baselineskip}\\
*"Connected" sympathy may only be removed with Unmaking spells. When affecting links with magic, each connection is treated as a subject for purposes of spell factors. For example, a spell that dampens a sympathetic connection can dampen multiple connections by increasing the Scale factor.\vspace{\baselineskip}\\
\textbf{Temporal Sympathy:} Strong \\
\textbf{Withstand (Connection):} 3\\
\textbf{Withstand (Temporal Sympathy):} 1\\
\textbf{Description:} The two subjects are closely metaphysically linked; a woman and her lifelong spouse, a mage and his dedicated magical tool or an item she has imbued, a Legacy mentor and her student, best friends, parents, children, bodily samples (blood, locks of hair,) murder weapons.\vspace{\baselineskip}\\
\textbf{Temporal Sympathy:} Medium\\
\textbf{Withstand (Connection):} 2\\
\textbf{Withstand (Temporal Sympathy):} 2\\
\textbf{Description:} The two subjects are linked; a mage and her own spells, items marked with a mage's Signature nimbus, friends, siblings, lovers, items of emotional significance like medals, wedding rings, a soldier's weapon, or a sportswoman's bat.\vspace{\baselineskip}\\
\textbf{Temporal Sympathy:} Weak\\
\textbf{Withstand (Connection):} 1\\
\textbf{Withstand (Temporal Sympathy):} 3\\
\textbf{Description:} The two subjects have barely touched one another metaphysically; the subjects of a mage's spells, or items she has used as Yantras. Casual acquaintances, coworkers, replaceable belongings.\\

\spell{Divination }
{Knowing}
{Potency}
{}
{}
{The mage can look into her subject's most likely future. Without Reach, the mage can only see generalities: "Will I meet Anna again soon?" is a valid question, while "What time will the police arrive?" is too specific to return an answer. This spell can see far into the future, such as telling the mage that a young cashier might eventually become a state Governor, or that a child prodigy might become a superstar, but looking too far from the present increases the likelihood of the answer being superseded by the point the future becomes the present. The Storyteller must decide what the future holds, taking into account the nature of the story as well as cues from the mage's questions. The caster can ask one general question per level of Potency, receiving answers of "Yes," "No," or "Irrelevant."
\reach[1]{The mage can ask more specific questions and receive mostly accurate answers. The answers need not come in the form of "Yes," "No," or "Irrelevant." Instead the mage can ask questions like "Will Anna marry?" or "Will I ever bear a child?" and receive more information, such as "Anna will meet an old flame, Jason, and the two will reunite and one day marry."}
}

\spell{Green Light / Red Light}
{Compelling}
{Duration}
{}
{}
{The mage may manipulate the subtle timings of events, smoothing or obstructing her subject's progress. Cast positively, the subject finds elevators and taxis arrive just as he needs them, stop lights turn green, and he arrives on time for meetings. Cast negatively, anything that can delay the subject will delay him.
}

\spell{Momentary Flux}
{Knowing}
{Potency}
{}
{}
{The mage can sense whether a subject will prove beneficial or baneful in the most likely future. The mage can see whether the stranger crossing the street to approach her at night is as threatening as he seems, or whether he has come to offer advice, for example. The spell itself does not tell the mage exactly what will happen, only whether it will prove good or bad for her. The stranger may augur as a bad omen for her; this could be due to his malicious intent - or maybe he's running from some danger, or even carrying a cold. Although often used to assess potential dangers, mages can cast this spell with themselves as the only subject, assessing whether their own actions will help or harm them. \\
Mages with Time $\spelldots{2}$ may use the Temporal Sympathy Attainment to cast this spell on a subject in the past, but the spell still reveals positive or negative outcomes for the future. The spell is Withstood by temporal sympathy.
\reach[1]{When reacting to the information gained from this spell, the mage gains a bonus to Initiative equal to Potency.}
}

\spell{Perfect Timing}
{Unveiling}
{Duration}
{}
{}
{The mage knows just the perfect time to act, whether it's with a kind (or condemning) word, a punch, or even simply slipping out a door at the right time. This spell does not directly alter time or affect others, but rather grants the subject a perfect temporal assessment of the situation. Others might describe her as "in the zone," mistaking her preternatural sense of timing for incredible focus. The subject can spend a turn during the spell's Duration planning an action. The subject loses any Defense and must remain still while planning. A turn spent planning grants a bonus to the next action equal to Potency. This bonus can only be applied to mundane instant actions; extended actions and spellcasting rolls do not benefit from it.
}

\spell{Postcognition }
{Unveiling}
{Potency}
{}
{}
{The mage can see into the past of a subject, witnessing events as though she were physically present to view them. By default, the caster may only view Unchanged subjects, but with Time $\spelldots{2}$ she may view the more distant past, in which case the spell is Withstood by temporal sympathy. The mage views the subject in "real time" from a moment declared when casting until the Duration of Postcognition expires. While viewing the past, the mage loses all Defense and may not take any actions or cast further spells.
\reach[1]{The mage can "scrub" the vision like a video, speeding it up, slowing it down, rewinding it, pausing it, etc.}
\reach[1]{The mage remains aware of her surroundings, and does not lose Defense.}
}

\spell{Choose the Thread}
{Ruling}
{Potency}
{}
{Resolve}
{Glimpsing the many potential futures of her subject, the mage selects the optimal course. The subject's player rolls twice for her next mundane dice roll, and the mage's player selects which dice roll takes effect.
\reach[2]{The spell affects spellcasting rolls and supernatural powers.}
}

\spell{Constant Presence}
{Shielding}
{Duration}
{}
{}
{Mages versed in Time know the tell-tale signs of disruption to the Patterns of time travelers, and not many Awakened are willing to trust that a traveler has honorable intentions. This spell preserves its subject against alterations to the timeline. Any alteration to history through the action of time travel provokes a Clash of Wills. If the mage wins, the subject is treated as though she were returning from a trip to the past herself when history settles, safeguarding her against being rewritten.
}

\spell{Hung Spell}
{Ruling}
{Duration}
{}
{}
{With this spell, the mage captures pure expressions of magic - spells - at the moment they enter the Fallen World, suspending them before they take effect but preserving their Duration from decay. The subject must be a mage, who must deliberately build his Imago to take advantage of this spell's effect and pay one Mana when casting. Hung Spell may entrap up to its Potency in spells, which remain in their caster's spell control but do not take effect until Hung Spell is canceled or runs out of Duration. When Hung Spell ceases, the trapped spells immediately take effect and begin their own Durations. Many mages use the Fate 2 Attainment to set a Conditional Duration on Hung Spell, so that it releases the trapped spells in response to set conditions.
}

\spell{Shield of Chronos}
{Veiling}
{Duration}
{}
{}
{The mage shields a subject against temporal senses. While under the protection of this spell, any magic viewing the subject through time (whether looking at the shielded Duration from the future, or predicting the subject's future while in the present) provokes a Clash of Wills.
\reach[1]{Instead of simply preventing magic from uncovering the subject during the Duration of the shield, the mage may design a false series of events that inquisitive powers "uncover" instead of the truth. Attempts to magically discern the illusion provoke a Clash of Wills.}
}

\spell{Tipping the Hourglass}
{Ruling}
{Potency}
{Resolve}
{}
{The mage can momentarily alter the flow of Time, causing it to speed up or slow down for a subject, but not drastically. While the spell might allow the subject extra time to dodge an oncoming car or slow an enemy's movements as though he were drunk, it won't let her go back in time to avoid the car or the angry assailant entirely. The caster may add or subtract Potency from the subject's Initiative. Subjects who have already acted in a turn before having this spell cast upon them do not act again on their new Initiative rating.
}

\spell{Veil of Moments}
{Shielding}
{Duration}
{}
{}
{The mage can ward off the deleterious effects of advancing time on her subject. This spell cannot undo effects, but can create enough of a buffer between the subject and Time's endless march to buy what a mage needs most - time to think. While this spell is active, the subject becomes immune to things that worsen with time. She will not bleed out from her wounds, and poison and toxins effectively halt their duration, as does the progression of disease. New Conditions and Tilts cannot be imposed on the subject while the spell remains in effect. Supernatural powers that impose effects provoke a Clash of Wills. The downside of the spell's protection is that the subject no longer heals naturally during the spell's Duration. More dramatic effects such as Pattern Restoration or Life magic can still heal her. More importantly, the subject cannot regain Willpower or Mana, or spend Experiences while under the spell's effect. The subject ceases aging during the use of this spell.
\reach[1]{The subject may ignore Persistent Conditions during the spell's Duration. Time spent under this spell's effects does not count toward any time necessary for Conditions to lapse.}
\reach[1]{ The subject may heal naturally while under the spell's effect.}
\reach[1]{The subject may regain Willpower while under the spell's effect.}
\reach[1]{The subject may regain Mana while under the spell's effect.}
}

\spell{Acceleration }
{Perfecting}
{Potency}
{One Mana}
{}
{The mage can greatly accelerate her subject's temporal motion. From the perspective of onlookers she becomes a blur as if moving in fast motion, acting with impossible speed. At high enough levels, mundane creatures simply cannot perceive her at all, save perhaps for hair raising on the neck or a gut feeling that something is not quite right. Multiply the subject's Speed by Potency. While under the spell's effect, the subject always goes first in a turn unless he chooses to delay his action, in which case he may interrupt any other character's turn with his own as a reflexive action, then return to the front of the Initiative queue the next turn. Other characters using pre-empting powers provoke a Clash of Wills. Acting in such accelerated time makes the subject very hard to hit, but only as long as he is able to concentrate; his Defense does not change, but add Potency to Defense before doubling it for Dodge actions (p. 217). He may employ Defense (and Dodges) against firearms.
\reach[1]{Divide the time per roll of extended actions taken by the subject by Potency. The spell has no effect on the ritual casting interval of mages.}
\reach[1]{For a point of Mana, Dodge actions taken while under this spell's effect have the rote quality.}
}

\spell{Chronos' Curse}
{Fraying}
{Potency}
{One Mana}
{Stamina}
{The mage slows his subject's experience of time to a crawl. To the subject, everything seems to move at dazzling speeds, while she feels like she's caught in a dream, unable to run or punch or move properly. She can't even speak normally to others while affected - while from her perspective her words are clear enough, to everyone else they're a long, impossibly drawn-out sound. Divide the subject's Speed by Potency, rounding down. If Speed reaches 0, the subject is effectively moving so slowly she appears rooted to the spot. While under the spell's effect, the subject always goes last in a turn. The subject's Defense is also reduced by Potency.
\reach[1]{For a point of Mana, the subject loses Defense against attacks.}
\reach[1]{Multiply the time per roll of extended actions taken by the subject by Potency. The spell has no effect on the ritual casting interval of mages.}
}

\spell{Shifting Sands}
{Fraying}
{Potency}
{}
{}
{The mage may step back through time a short distance, undoing a few precious seconds. This spell sends the subject back through time a number of turns equal to Potency. The subject retains any injuries and Conditions gained in the undone turns, and spent Mana and Willpower do not return. Spells cast on her person in the undone time remain as long as she cast them. All other spells she may have cast or had cast on her in the intervening time are canceled. Until the subject catches up to the present, the distortion caused by this spell is visible under Active Time Mage Sight. Once she does so, any changes she made to history become Lasting.
\reach[1]{The subject travels back a full scene. This Reach effect may be applied multiple times.}
}

\spell{Temporal Summoning}
{Weaving}
{Potency}
{}
{}
{We are all the sum of our previous selves, and with this spell a mage can call those afterimages forth. Cast upon an object, area, or living being, this spell replaces its subject with an earlier version of itself, chosen by the caster. Without the Temporal Sympathy Attainment, only Unchanged pasts may be brought to the present, but this is still sufficient to remove most Conditions and heal injuries. By employing Temporal Sympathy, the mage can restore ancient ruins to their inhabited state or return her enemies to childhood. When the spell's Duration ends, the subject immediately returns to its present self. Injuries, Conditions, and other effects imposed on the subject while Temporal Summoning is in effect transfer to the present version of the subject when it returns. The spell does have limits, however; it cannot bring the dead back to life or undo transformation into supernatural beings, though it still changes the subject physically - a vampire returned to "childhood" becomes a vampire child, and a corpse stripped of decades becomes a younger looking corpse with no cause of death.
}

\spell{Weight of Years}
{Perfecting}
{Potency}
{}
{}
{Structures decay, bodies age. Toxins build up in muscles, and materials become brittle. The mage can inflict these processes by Perfecting the passage of time on her subject. This is an attack spell, inflicting its Potency in damage to objects and structures. This damage directly affects the object's Structure, and reduces its Durability by 1 for every 2 points of Structure lost. When used against living things, the spell deals bashing damage equal to Potency. At the Storyteller's discretion, immortal creatures like vampires might be immune to the damaging properties of this spell.
\reach[1]{The spell reduces living subject's Athletics by Potency through sheer exhaustion.}
}

\spell{Present as Past}
{Patterning}
{Potency}
{1 Mana}
{}
{Weaving between the many immediate potential futures, the mage can read the immediate futures of her subjects and react accordingly to thwart (or aid) their plans. In combat, while this spell is in effect, the player can require that every character affected by the spell declare his or her action at the start of every turn. The player need not declare her own action, but instead can choose to act freely at any point within the Initiative order. This trumps all other supernatural Initiative effects save for those created by the Time Arcanum, which requires a Clash of Wills. In Social situations, the mage adds Doors equal to Potency when the target of Social maneuvering by her subject, or removes them from a subject she is maneuvering against.
}

\spell{Prophecy}
{Patterning}
{Potency}
{}
{}
{The mage causes the future to conform to her expectations, building a hypothetical scenario she can then examine for knowledge about how to alter the future drastically, whether to ensure or avert a specific event. This works like "Divination," p. 186, but the mage can ask specific questions and also gains answers about things that might come to pass, depending on variables like choice or outside chance. For example, she could ask whether calling her ex will lead to reconciliation if she makes the attempt, or whether killing a man might set his son down a road to revenge. She can ask one such question per level of Potency and receive a detailed answer that accounts for hypothetical events. Other mages using Divination on the same subject while Prophecy is in effect see the most likely outcome of the scenario set by this spell.
\reach[1]{By building the hypothetical future around a Social interaction, the mage may reduce the subject's Doors by Potency, since she has intimate knowledge how each question or interaction might affect the target's choices.}
}

\spell{Rend Lifespan}
{Unraveling}
{Potency}
{}
{}
{The mage can cause parts of a target's body to age rapidly and others to regress in development. The effects are temporary but devastating, inflicting lethal damage equal to the spell's Potency. Targets killed by this spell often appear to have "died of old age," despite their apparent age. At the Storyteller's discretion, undead beings like vampires and ghosts may be immune to this spell. 
\reach[1]{For a point of Mana, the spell now inflicts aggravated damage.}
}

\spell{Rewrite History}
{Patterning}
{Potency}
{}
{Resolve}
{Reaching back through time, the mage warps her subject's timeline, making her present self as though her life took a very different course. This spell allows the mage to rewrite a subject's history, choosing a point of divergence on his timeline and specifying changes from there. Without Temporal Sympathy, only recent decisions and changes can be rewritten, but as long as the subject is Unchanged at the point of divergence, the mage may make alterations as she wishes. With Temporal Sympathy, the spell is capable of changing every detail of a subject's history, though the false timeline created must still be possible. Once the Duration expires, the subject instantly reverts to its original history. Memories of the time spent "rewritten" will seem distant and hazy, dreamlike, but the subject will remember the altered perspective at least to some degree unless Mind magic further alters her memories. This spell does not normally affect supernatural creatures.
\reach[1]{The spell may reassign up to Potency Skill or Merit dots, as the subject's training and background shift. Skills may not exceed the subject's maximum.}
\reach[1]{The spell may reassign Attribute dots up to Potency, but may not move any category of Attributes below the character creation amount for its priority, or take any Attribute over the maximum for the subject.}
\reach[2]{This spell can affect supernatural creatures. While this cannot remove the supernatural Advantages of creatures born to their condition (like werewolves, spirits, or demons), creatures transformed at some point during their lives, like vampires or changelings, can briefly experience life as though they hadn't been changed.}
}

\spell{Temporal Stutter}
{Patterning}
{Potency}
{}
{Stamina}
{By redefining how a subject's Time Pattern interacts with the present, the mage throws that subject forward through time, awaiting the moment the present catches up to him. The subject completely vanishes from the Fallen World, and reappears unchanged when the spell's Duration ends. The subject experiences a momentary lurch in his perceptions, and then suddenly finds his surroundings changed by intervening events. The subject remains in the same location and retains momentum if he had been moving. If something now occupies the space the subject reappears in, apply the Knocked Down Tilt to whichever has the least Size.
\conjunction{Add Space}{2}{By using the Sympathetic Range Attainment tied to a destination, the spell brings the subject back at that destination rather than at the point he left.}
}

\spell{Blink of an Eye}
{Unmaking}
{Potency}
{}
{}
{By collapsing time around a subject, the mage allows her to accomplish in seconds what would take hours. This spell turns the next extended action taken by the subject into an instant action, absorbing rolls equal to Potency into a single turn. It does not affect ritual casting intervals for mages.
\reach[2]{For a point of Mana, the spell may affect spellcasting, increasing the effective Gnosis of a mage subject by Potency for purposes of ritual casting interval only. For every effective Gnosis over 10, reduce the ritual interval by one turn.}
}

\spell{Corridors of Time}
{Unmaking}
{Potency}
{}
{}
{Where less-advanced Time mages can only undo actions leading directly up to the present, a Master may choose any moment in her subject's timeline and destroy everything after it, sending the subject's present self back in time to the moment of the mage's choosing. The subject arrives in the past at the specified time and is free to act, changing history by his actions, although the distortions to his timeline are visible under Active Time Mage Sight. He remains in the past for a time equal to Corridors of Time's Duration factor, or until he "catches up" to the present. Once in the present, the new timeline sets and any changes the subject made to history become Lasting. Without temporal sympathy, the subject may only be sent back to a period with an Unchanged temporal sympathy to the present. By using temporal sympathy, the mage may allow her subject to revisit old decisions and make different choices. The subject arrives in whatever location he was in at the time chosen. By including the Sympathetic Range Attainment, the mage may send him somewhere else, but subjects cannot be sent back to periods outside their own lifetime. Such unfettered time travel is the stuff of legend, temporal Irises, and whispers about the powers of archmages.
Errata: The subject inhabits their own past self - there's not two of them running around.
}

\spell{Temporal Pocket}
{Making}
{Duration}
{}
{}
{The mage grants her subject a gift of hours, Making extra time on her timeline. To the subject, the entire world appears to pause, frozen in time. After the subjective Duration factor of the spell, the subject rejoins the Fallen World's timeline and, to him, the universe immediately starts moving again. While under the effects of a Temporal Pocket, the subject ages normally, any Conditions that change with time continue, wounds continue bleeding out, he must sleep the usual amount, and so on. He may move freely, examine objects, take any Mental actions, heal, touch things, and even cast spells with himself as the subject, but not physically move, consume, or injure anything - any attempt to do so immediately ends the spell, but returns the subject to the timeline having just completed the action he attempted.
}

\end{document}