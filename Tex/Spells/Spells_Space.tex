\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Space}

\section{Space}
\textbf{Purview:} Distance, separation, sympathy, conjuration, scrying, warding.\\
The gross Arcanum of Pandemonium expresses that physical separation is as much a lie as the isolation of the soul. Through this Arcanum, the mage can magnify or collapse the distance between places and objects, conjure things from distant locales, and twist the topography of space into strange and unnatural shapes. Space also allows a mage to manipulate the sympathetic connection between subjects.

\subsection{Keys}
Since the Space Arcanum's purview includes portals, doors, and similar liminal spaces, it's only natural that many Space spells allow the mage to specify a Key - a particular item carried, password spoken, or other criterion that either activates a spell or bypasses its effects. For example, a portal might only open on the summer solstice, or a hidden path might be visible to anyone of the mage's bloodline. Spells that can be Keyed are called out below, usually at the cost of a Reach. The Key may have a number of requirements equal to the spell's Potency.

\subsection{Sympathy}
Space allows a mage to manipulate, destroy, and create from nothing the occult bonds of sympathy. The concept of sympathy is a complex one, and the libraries of the Orders contain volumes on the subject, but briefly: Two subjects may become sympathetically linked when they share a strong emotional, physical, or mystical connection. Naturally-occurring links are sometimes permanent, but just as often fade over time: a brief but passionate love affair's link might fade within a few weeks of the breakup, while a murder weapon retains links to both killer and victim for years. Sympathetic links manipulated with the Space Arcanum echo from the Supernal to the Fallen, wreaking subtle but long-standing changes in the subjects. Sever a man's link to his husband and the relationship cools and grows distant. Create a bond between a woman and a gun, and she'll find herself thinking about it, dreaming about it - and left to their own devices odds are good woman and gun will cross paths. This isn't precisely mind control, or even an expression of Fate magic: all Space can do is manipulate the connections, not control reactions or guarantee outcomes. Maybe the chilled couple seeks counseling and repairs their relationship. Maybe the woman tells herself she's being crazy and deliberately ignores thoughts of firearms. The point is that playing with people's sympathetic links has a real, tangible effect on their lives. The Wise look askance on those mages who endeavor to cull their own sympathetic "vulnerabilities" with good reason. Most spells dealing with sympathy use the connections themselves as the spells' subjects, and are Withstood by the  links' relative strength - deeper bonds are more difficult to affect. Mages with Space 2 or more can also use links to cast other spells on subjects not within sensory range, by routing their magic along the lines of sympathy using an Attainment. In these cases, the spell is Withstood by Space itself; weaker connections are more difficult to use. When affecting links with magic, each connection is treated as a subject for purposes of spell factors. For example, a spell that dampens a sympathetic connection can dampen multiple connections by increasing the Scale factor

\subsection{Sympathetic Names}
In the womb, a human being has only a Connected link to her mother and a Strong one to her genetic father. At birth, this shifts to a Strong link to both parents and other primary caregivers, whether related or not, but babies don't otherwise form sympathetic links until the sense of self develops. Once the Lie sets in that the world is separate from the self, a child begins to form her own links. Even in adulthood, everyone's sympathetic links bear a permanent reminder of that early time - the name that those with links to a baby think of her as becomes indelibly marked in her Pattern as her sympathetic name. No matter how many times a person changes her name through childhood and adulthood, once she forms links of her own the sympathetic name is set, and is both weakness and protection - speaking a person's sympathetic name counts as a Yantra as per p.119, but not knowing it makes affecting her at range more diffiult, increasing the Withstand level against the Sympathetic Range Attainment by one.

\newpage
\subsection{Sympathy}
%\begin{table}[!ht]
%\tiny
%\setlength\tabcolsep{1.5pt} 
%\begin{tabular}{p{0.18\columnwidth}p{0.4\columnwidth}p{0.2\columnwidth}%p{0.2\columnwidth}}
%	\textbf{Sympathetic Strength} & \textbf{Description} & \textbf{Withstand (Connection)} & \textbf{Withstand (Sympathetic Range)}\\
%	Connected & The two subjects are metaphysically one e.g. a mage and her familiar or soul stone. The connection is unassailable without Unmaking magic and casting using the connection is not Withstood. & 5* & 0 \\
%	Strong & The two subjects are closely metaphysically linked; a woman and her lifelong spouse, a mage and his dedicated magical tool or an item she has imbued, a Legacy mentor and her student, best friends, parents, children, bodily samples (blood, locks of hair,) murder weapons. & 3 & 1 \\
% \end{tabular}
% \end{table}
\textbf{Sympathetic Strength:} Connected\\
\textbf{Withstand (Connection):} 5*\\
\textbf{Withstand (Sympathetic Range):} 0\\
\textbf{Description:} The two subjects are metaphysically one e.g. a mage and her familiar or soul stone. The connection is unassailable without Unmaking magic and casting using the connection is not Withstood.\vspace{\baselineskip}\\
*"Connected" sympathy may only be removed with Unmaking spells. When affecting links with magic, each connection is treated as a subject for purposes of spell factors. For example, a spell that dampens a sympathetic connection can dampen multiple connections by increasing the Scale factor.\vspace{\baselineskip}\\
\textbf{Sympathetic Strength:} Strong \\
\textbf{Withstand (Connection):} 3\\
\textbf{Withstand (Sympathetic Range):} 1\\
\textbf{Description:} The two subjects are closely metaphysically linked; a woman and her lifelong spouse, a mage and his dedicated magical tool or an item she has imbued, a Legacy mentor and her student, best friends, parents, children, bodily samples (blood, locks of hair,) murder weapons.\vspace{\baselineskip}\\
\textbf{Sympathetic Strength:} Medium\\
\textbf{Withstand (Connection):} 2\\
\textbf{Withstand (Sympathetic Range):} 2\\
\textbf{Description:} The two subjects are linked; a mage and her own spells, items marked with a mage's Signature nimbus, friends, siblings, lovers, items of emotional significance like medals, wedding rings, a soldier's weapon, or a sportswoman's bat.\vspace{\baselineskip}\\
\textbf{Sympathetic Strength:} Weak\\
\textbf{Withstand (Connection):} 1\\
\textbf{Withstand (Sympathetic Range):} 3\\
\textbf{Description:} The two subjects have barely touched one another metaphysically; the subjects of a mage's spells, or items she has used as Yantras. Casual acquaintances, coworkers, replaceable belongings.\\


\spell{Correspondence }
{Knowing}
{Potency}
{}
{}
{We are all of us defined by our connections, and through this spell a mage learns those definitions. For every level of Potency, the mage learns one of the subject's sympathetic links. The spell reveals the subject's oldest and strongest links first. She understands these connections in the same manner the subject thinks of them (e.g. "my childhood home," not "1414  Willowbrook Drive, Columbus, OH"). If the other half of the sympathetic link is within the mage's sensory range, she knows that and knows its exact location.
\reach[1]{The mage can "follow" a trail of sympathetic links. When she learns of one of her initial subject's sympathetic connections, she may choose to learn one of that subject's connections, and so on. For example, she might follow a wedding ring to its owner, then its owner's spouse, then the spouse's place of employment.}
\reach[1]{The mage understands the emotional character of the connection in broad terms. "My childhood home" might carry notes of comfort and safety or fear and loathing, depending on the subject's upbringing.}
\reach[2]{The mage may specify the connections she wishes to uncover in general terms ("an object with a strong link to his childhood") or ("those she hates"). Again, the knowledge is contextualized based on the subject's perceptions.}
\reach[2]{If the subject is a Keyed spell or Iris (p. 243), the mage may use one level of Potency to discover the Key instead of a link.}
}

\spell{Ground-Eater}
{Compelling}
{Potency}
{}
{Stamina}
{Space is more flexible than many believe. By subtly pinching or stretching the space around her subject, the mage allows her to cover far more ground with each step than is readily apparent. The mage adds the spell's Potency to the subject's Speed. Watching someone under the influence of this spell is alarming: It's hard for the eye to track her, as each step carries her farther than it should, and in every blink or momentary glance away she seems to leap farther than should be possible in such a short time. This spell can also reduce a subject's Speed by its Potency (though not below 1). Those who have experienced this effect liken it to a nightmare wherein no matter how fast you run, you never get closer to your goal.
}


\spell{Isolation }
{Compelling}
{Potency}
{}
{Composure}
{Boundaries and barriers are a lie, but it is sometimes useful to lie. This spell subtly warps space and distance around the subject, making empty spaces seem larger and more foreboding. Crowds of people seem tightly packed together, an impenetrable wall of humanity. Any attempt the subject makes to interact with other people costs 1 Willpower. Even then, any dice pools are penalized by the spell's Potency. Prolonged exposure to this spell (roughly a day per dot of the subject's Composure) may provoke breaking points or adverse Conditions like Shaken or Spooked.
}

\spell{Locate Object}
{Knowing}
{Duration}
{}
{}
{All distance is an illusion. Once this truth is understood, all things are in the same place as the mage, and how can one lose track of herself? As long as the subject of this spell is within the Area of the spell, she knows its precise location. Short of concealing magic (which provokes a Clash of Wills), no attempt to hide the subject can fool her unerring senses.
\reach[1]{ The caster can continue tracking the subject even if it leaves the Area.}
}

\spell{The Outward and Inward Eye}
{Unveiling}
{Duration}
{}
{}
{If all locations are one, it must follow that all directions are one as well. While this spell is active, the subject can see and hear in all directions and from all points within her sensory range simultaneously. She can see what's happening behind her, on the far side of a door, or beneath her feet. She cannot perceive things farther away than her normal perceptions might allow, nor can she see through darkness. In essence, it's as though everything happening around her were spread out on a flat plain, bereft of obstruction. This allows her to cast sensory-range spells on subjects she might not ordinarily be able to perceive. The subject is also nearly impossible to ambush or surprise - barring exceptional camouflage or a tremendous distraction to draw her attention, all such attempts are reduced to a chance die. Finally, the subject may reduce any penalties due to range, cover, or concealment (but not darkness or similar poor visibility) by the spell's Potency.
\reach[2]{The mage may use the spell to see through an existing warp or shortcut through Space; a Distortion Iris, the effects of a Scrying spell, or a magical portal created with Co-Location are all applicable. With additional Arcana based on the nature of the portal, other kinds of Iris may be seen through at the Storyteller's discretion.}
}

\spell{Borrow Threads}
{Ruling}
{Duration}
{}
{Connection}
{By changing one's connection to others, one changes oneself. This spell allows the mage to transfer a number of sympathetic connections equal to the spell's Potency between herself and the subjects as determined by the spell's Scale. She can either steal links from her targets or give her own to others. If the mage transfers a link to someone who already has a connection to the same thing, the new connection overwrites the old one for the Duration of the spell. The mage has to be aware of a connection (either through magic or just knowing the subject) to manipulate it.
\reach[1]{The mage may redirect the sympathetic connection between other subjects of the spell directly.}
\reach[1]{Instead of transferring sympathetic connections, the mage may copy them.}
}

\spell{Break Boundary}
{Ruling}
{Potency}
{}
{}
{To restrain a willworker is to try to ensnare someone in a lie he does not believe. This spell allows the mage to bypass a single obstacle restricting her subject's movement: a locked door, a pair of handcuffs, a barred window, etc. The subject "blinks" through the door, or her hands seem to pass right through the handcuffs, or similar effects. This spell can only bypass a physical obstacle obstructing an actual path. The mage can, for example, slip through a roaring fire that blocks the road ahead or across a chasm too wide to jump, but cannot blink through a solid wall. If cast on an inanimate object, the mage or an ally must still carry or push the subject through the obstacle.
\reach[1]{The spell allows subjects to fit through narrow or restrictive pathways they would not otherwise pass through, even if the path were not obstructed. For example, casting this spell on a car would allow the mage to drive it inside a house even though it would not fit through the door.}
\reach[2]{Subjects pass through obstructions even if they are unable to move, appearing on the other side.}
}

\spell{Lying Maps}
{Veiling}
{Duration}
{}
{Resolve}
{To know how to get from here to there is to tacitly accept the Lie. By means of this spell, the mage twists the subject's sense of direction, making him certain that the best route from where he is to somewhere else is one the mage desires. She could, for instance, convince the subject that the road to a dangerous Verge is actually the way to his mother's house, or that the mage's sanctum is in another part of the city. If the subject actively, carefully navigates using a map or GPS or the like, the navigation roll is a single chance die, and even on a success it feels wrong. ("The map says left, but I swear it's right!")
}

\spell{Scrying }
{Ruling}
{Duration}
{}
{}
{By means of this spell, the mage parts the curtain of the Lie and reveals a distant location to her senses. She creates a "window" that allows her to perceive the subject, much like a television screen. When she casts the spell, she may choose whether the spell is one way, or whether people at the location can see back through the window. When casting this spell sympathetically, exactly what the mage sees depends on the sympathetic Yantra she employs. Sympathy to a location shows her a broad overview of the area, analogous to a cinematic wide shot, but one that remains static. Sympathy to a person or object tends to show a "close up" of the subject - the window will follow the subject if it moves, but the mage may not be able to make out details of where the subject is or who else might be present. When cast on a subject within sensory range, the scrying window gives the mage a view as though she were standing right next to the subject. The mage can move this view around as a reflexive action to view the target from any angle she wants. Casting spells on subjects the mage can see through the window counts as viewing them remotely.
\conjunction{Add Fate}{2}{The mage may make the scrying window selectively one-way, allowing only specific people to perceive her from the other side of the window.}
}

\spell{Secret Door}
{Veiling}
{Duration}
{}
{}
{Doorways, roads, and portals represent a liminal point between two distinct locations - but if distance is an illusion, there can be no "distinct locations." This spell cloaks a door, intersection, or similar aperture between two locations, such that one's mundane perceptions simply slide ride past it. All magical attempts to uncover the door provoke a Clash of Wills.
\reach[1]{The mage may specify a Key that allows the Secret Door to be seen.}
}

\spell{Veil Sympathy}
{Veiling}
{Duration}
{}
{Sympathy (Connection)}
{A magician's sympathetic connections allow her to reach out beyond herself, but they are also an avenue by which her enemies can attack her. This spell conceals one of the subject's sympathetic links, chosen by the mage from those she is aware of. Any attempt to uncover or use the link provokes a Clash of Wills.
\reach[1]{Rather than suppressing a sympathetic link, the mage may instead make the subject appear to have a link to someone or something else instead. Attempts to detect the link provoke a Clash of Wills to see through the deception.}
\reach[1]{The spell prevents the subject from being used as a sympathetic Yantra (p. 122) as well as removing its links; each level of Potency reduces all potential uses of the subject as a Yantra from Material, to Representational, to Symbolic, to suppressing them entirely.}
\reach[2]{The mage may suppress all the subject's sympathetic links. This effect applies in both directions; not only does the subject lose his links to loved ones, but those people lose their links to him. The spell is Withstood by the strongest connection the subject has.}
}

\spell{Ward}
{Shielding}
{Duration}
{}
{}
{Space is mutable, until a magician wills otherwise. Cast on an area or individual subjects, this spell locks its subject down, preventing the space within from being manipulated. Magic that uses the sympathy of Warded subjects or attempts to warp Warded areas provokes a Clash of Wills. The mage is aware when one of her Wards is attacked.
\reach[1]{The mage may specify a Key that allows use of Space magic on the Warded subject.}
\reach[2]{The mage may cast Ward upon an Iris (p. 243) or its Key, preventing the Iris from opening while the Ward remains in effect. Supernatural powers opening the Iris provoke a Clash of Wills.}
}

\spell{Ban}
{Weaving}
{Duration}
{}
{}
{In the quest for self-knowledge, it is sometimes useful to cut oneself off from the outer world so that one can understand that the world is contained within. By means of this spell, the mage inverts an area of space, such that nothing inside the space can get out and nothing outside the space can get in. Try to step in and you find yourself on the far side, carried in a single step. Try to get out and you're just stepping right back in again. Magic that manipulates space, like a teleportation power or the ability to step from one world to another, provokes a Clash of Wills to allow ingress or egress. Even light and air can't pass through: From the outside, the space appears to "lens" as the observer approaches it, as light jumps directly across the Ban. From inside, it's an island of light in a vast sea of darkness.
\conjunction{Add Any Arcanum}{2}{Either exclude one or more phenomena under the Arcanum's purview from the spell (for example, to let air or light through) or create a Ban that only prohibits phenomena under that Arcanum's purview.}
}

\subsection{Since We Know You're Thinking It...}
The average adult human being at rest consumes about one cubic foot of breathable air every four minutes. Have fun.

\spell{Co-Location}
{Fraying}
{Duration}
{}
{}
{Where lesser spells merely distort the Lie that all things are separate, this spell attacks it directly. The mage smears the distance between a number of locations equal to the spell's Potency, causing them to overlap temporarily. The mage must employ the Sympathetic Range Attainment to overlap locations outside of her sensory range. Only mages using Active Mage Sight with Space can perceive the overlap, seeing it as a confusing jumble of translucent images constantly interpenetrating each other; to others within the affected areas everything seems normal. The spell's Scale factor determines how large each overlapped area can be. Each turn, as a reflexive action, anyone capable of perceiving the overlap may "move" an object, person, or other being she is touching (including herself, if desired) from one location to another, effectively teleporting it from place to place. Those capable of seeing the overlap can touch things in any of the co-located areas, but can otherwise only interact with the location they are physically in. The other location counts as being viewed remotely for purposes of further spellcasting, and individuals may not attack people in different locations. Objects cannot be teleported into other objects with this spell.
\reach[1]{The mage can make anything in the overlapped locations visible. She may specify any individual object or person, or just make an entire location appear as she wishes. The contents of locations remain insubstantial to one another, however anyone may "move" an object he can see over, not only individuals with Space senses.}
\reach[1]{The mage can restrict the co-location to a two-dimensional plane, creating a stable portal between two locations. Anyone capable of perceiving the portal may pass through it. It is invisible by default, but by combining this with the above Reach effect the mage may make it visible and useable by individuals without Space senses.}
\reach[1]{The mage may specify a Key needed to use the overlap.}
\reach[2]{Individuals capable of perceiving the overlap may reflexively switch locations twice per turn.}
}

\spell{Perfect Sympathy}
{Perfecting}
{Duration}
{}
{}
{To possess true Sympathy toward something is to be nearly indistinguishable from it. With this spell, the subject becomes so like those with whom she has sympathy that she finds it trivial to predict them. When the subject takes an action whose subject is one of her Strong sympathetic connections (e.g. social interaction, guessing what he'll do in a certain situation, etc.), she gains 8-Again on her roll.
\reach[1]{The subject's sympathetic connection is so great that it can fool even magic. When the subject of Perfect Sympathy is the target of a spell using the Sympathetic Range Attainment, it provokes a Clash of Wills. If the mage succeeds, she may redirect the spell's effects to one of the subject's Strong sympathetic connections instead.}
\reach[1]{For one point of Mana, the subject gains the rote action quality on a number of actions equal to the spell's Potency, as long as those actions affect one of her Strong sympathetic connections.}
\reach[1]{The benefits of this spell extend to the subject's Medium sympathetic connections.}
}

\spell{Warp }
{Fraying}
{Potency}
{}
{}
{The mage twists the space her subject occupies, torquing joints, bruising flesh, and tearing muscle. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts bashing damage.
\reach[1]{The pain of the attack is such that the victim gains the Arm Wrack or Leg Wrack Tilt.}
}

\spell{Web-Weaver}
{Perfecting}
{Duration}
{}
{Composure}
{We all leave tiny, nigh-imperceptible webs of sympathy behind us wherever we go. With this spell, the mage may bolster such a web into a useful sympathetic link. Each level of Potency bolsters a single sympathetic connection by one "step," from Weak to Medium, Medium to Strong. The mage can step up a nonexistent connection to a Weak one, but only if the subject of the spell has been in contact with the desired focus within the last turn. For example, a person likely has no sympathetic connection to the soda cup from her lunch, but as long as it's in her hand, the mage can use the faint sympathy created by physical contact to make the cup a sympathetic connection. 
\conjunction{Add Time}{2}{The mage can employ Temporal Sympathy (p. 193) to bolster nonexistent connections to anything the subject touched in the target time.}
}

\spell{Alter Direction}
{Patterning}
{Duration}
{}
{}
{"Direction" is nothing more than a vector between two points. With this spell, the mage overwrites that concept, letting her define her path as anything she desires. When cast on an area, this spell allows her to change a number of absolute directions (e.g. north, south, up, down) equal to the spell's Potency. She might redefine "down" as "up," causing anything not rooted to the ground to fall into the sky, or redefine "north" as "south by southwest," causing compasses to point the wrong way. Objects entering the area at speed find their direction of travel and momentum abruptly altered, which may require a Dexterity + Athletics or Drive roll to maintain control. This change isn't necessarily reciprocal; if the mage decrees that north is south, that doesn't mean that south is north - rather, it is impossible for anyone in the area to go north. Alternately, the mage may cast this spell on a specific subject and change a direction relative to that subject. She might redefine her own personal "down" as "the direction my feet are pointing," allowing her to walk on walls or ceilings, or redefine an attacker's "forward" as "toward the person holding the gun" before he shoots her.
\reach[1]{ The mage can redefine directions as curves, loops, or other shapes beyond a simple straight line. This might allow her to cause anyone walking straight ahead to move in an endless circle, or trace a "straight" path for her bullets that weaves around obstacles and friendlies. Apply the spell's Potency as a bonus or penalty to relevant actions.}
}

\spell{Collapse}
{Unraveling}
{Potency}
{}
{}
{While lesser magic can blur the distinction between locations, this spell can destroy them outright. The mage forces her subject to momentarily occupy the same space as another object, with catastrophic effects. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts lethal damage. Collapsing multiple subjects into each other, thereby damaging them all, is an application of increased Subject Factor.
\reach[1]{For one point of Mana, the damage is aggravated.}
\reach[1]{The co-located object remains inside the subject, preventing natural or magical healing of the wound until it's surgically excised or ripped out.}
}

\spell{Cut Threads}
{Unraveling}
{Potency}
{}
{Sympathy (Connection)}
{Isolation is the beginning of understanding. This spell destroys one of the subject's sympathetic links (additional connections can be severed by increasing the number of subjects with the Scale factor). This effect is Lasting, but normal interactions may restore the links in time, as described on p. 172.
\reach[2]{The mage may remove the subject's sympathetic name. This effect is not Lasting; the name returns when the spell's Duration expires.}
}

\spell{Secret Room}
{Patterning}
{Duration}
{}
{}
{Volume is a product of dimension, and dimension is merely an expression of distance in three dimensions. This spell allows the mage to manipulate those axes, making a space much larger or smaller than should be possible. A cramped studio apartment can become a spacious loft, or a town square can be made the size of a closet. Subjects crushed by a shrinking space too small for them take lethal damage equal to the spell's Potency and are forcibly ejected from the space. The Scale spell factor must encompass the area as it exists before the spell acts. The subject space's volume is increased or decreased a number of steps along the Area Scale factor table equal to the spell's Potency. Anyone or anything within the expanded space when the spell runs out simply appears outside the original, unaltered space.
}

\spell{Teleportation }
{Patterning}
{Potency}
{}
{}
{By means of this spell, the mage transforms her subject's current location, effectively moving it from point to point without crossing the intervening space. She can, for example, summon a subject to her from anywhere in the world, banish someone to the outer reaches of Siberia, or teleport herself. By default, the subject's current location and destination must both be within
sensory range, but the mage may employ the Sympathetic Range Attainment on one of them.
\reach[1]{The mage can "swap" the location of two subjects with no more than one point of Size difference.}
\reach[2]{The mage can now cast the spell with two separate Sympathetic Ranges, teleporting subjects without being present for either end of the journey. The spell is Withstood by the worse Sympathetic link.}
\conjunction{Add Death or Spirit or Other}{2}{By adding $\spelldots{2}$ in an Arcanum whose purview includes another realm of existence (e.g. the Underworld or the Shadow), the mage may move things from that realm into the physical world, or vice versa.}
}

\spell{Create Sympathy}
{Making}
{Potency}
{}
{Connection of desired link}
{To a Master of Space, powerful connections are forged as easily as snapping one's fingers. With this spell, the mage creates a new sympathetic connection on the subject. These new connections are Lasting, but can fade with time as described on p. 172.
\reach[1]{The sympathetic connections created are Lasting, and will never fade. Only magic can sever them. This can have dramatic, long-term psychological repercussions on a subject, as he is effectively never able to emotionally let go.}
\reach[2]{The mage may apply a new sympathetic name to the subject, which does not replace the original. This effect is not Lasting, and fades when the spell's Duration ends.}
}

\spell{Forge No Chains}
{Unmaking}
{Duration}
{}
{}
{To leave behind attachments is the truest sign of freedom. For the Duration of this spell, the subject leaves no sympathetic traces behind. She cannot forge sympathetic connections, and even blood, hair, and the like shed during the spell's Duration do not link back to her. Her Space spells leave no tell-tale ripples in the Tapestry. Any attempt to scrutinize her Space magic or previously-created sympathetic connections with Mage Sight (see p. 92) add the spell's Potency to the Mystery's Opacity.
}

\spell{Pocket Dimension}
{Making}
{Duration}
{}
{}
{The mage creates a space outside of space, one ideally suited to serve as a sanctum - or a prison. Without the addition of other Arcana, this space is devoid of any identifiable features, dimensions, or boundaries. It has no Time, so anything within it is held in stasis, unaging (but also unhealing and never growing or improving). It has no Death or Spirit, so Twilight doesn't exist within it. It is, in essence, a space whose only definition is that it is a space. Someone within the dimension can walk forever in any direction, but when she turns back she finds herself only as far as the boundary of the spell's Area Factor. The Pocket Dimension is divorced from physical reality; unless the mage chooses to anchor the realm to a point in the world, the only way to reach it is to teleport there. Spells cast within the Pocket Dimension do not incur Paradox, unless they are cast sympathetically on someone outside the Pocket Dimension. The mage counts as a material sympathetic Yantra for her own Pocket Dimension. If the Pocket Dimension is ever destroyed, or if its Duration expires, everything inside reappears in the world at the exact location from which it or they entered the Dimension.
\reach[1]{The mage may create an Iris to the Pocket Dimension in the material realm, allowing anyone to enter and leave it. For an additional Reach, she may specify a Key for the Iris.}
\conjunction{Add Time}{2}{Time flows normally within the pocket dimension, matching the flow of Time in the material realm. Wise mages supplement this conjunction with Matter spells or similar effects to ensure a continuous air supply.}
\conjunction{Add Death or Mind or Spirit}{2}{The Pocket Dimension includes Twilight for entities attuned to the Arcanum added.}
}

\spell{Quarantine}
{Unmaking}
{Duration}
{}
{}
{This spell excises a subject from Space altogether, removing all paths from it to the rest of the world and vice versa. For all intents and purposes, the subject simply ceases to exist and reality "fills in" to adjust. A Quarantined house doesn't leave behind an empty lot; rather, its two neighboring houses suddenly find themselves adjacent. A building with a Quarantined 12th floor appears to only have 11 stories - though the elevator has a "12" button, it doesn't do anything. Those inside the Quarantined area find that they cannot leave - any attempt to do so simply loops back through whatever door they passed through. They are, in effect, in a Pocket Dimension - albeit one that, because it is actually an excised piece of the Fallen World, possesses its own Time, Twilight, and so forth.
\reach[1]{The mage may specify a Key that allows access to and from the Quarantined area.}
\conjunction{Add Mind}{4}{For as long as the Quarantine exists, no one remembers that the Quarantined area, or anyone caught within it, exists. Memories are altered as necessary (so the new neighbors remember always being neighbors, and the office workers are sure the "12" button in the elevator was a weird construction gaffe and there never was an advertising firm on 12, and so on).}
\conjunction{Add Time}{5}{For as long as the Quarantine lasts, not only is the targeted area excised from space, it retroactively never existed at all. Any influence it, or anyone inside, had on the world is either undone or was caused by someone else. Normal history reasserts itself when the spell ends.}
}

\end{document}