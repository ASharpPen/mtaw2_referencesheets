\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Fate}

\section{Fate}
\textbf{Purview:} Blessings, hexes, probability, fortune, oaths, promises, intentions, destiny.\\
Arcadia's ruling Arcana are Time and Fate, and Fate is the subtle expression of that pair. Fate describes what should or must happen, but not precisely when or how that result will come about (those are the province of Time). Fate governs blessings, curses, destiny, fortune, oaths, probability, luck, and intent.
Those who master it may seem lucky or carefree, but in reality they deal in the inevitable and learn to anticipate it - whether to accept the dictates of destiny or redirect the road they walk. 

\subsection{Hexes}
Many Fate spells hex the subject imposing one of the following effects:
\begin{itemize}
\item Impose a dice penalty equal to Potency to all mundane actions the subject takes for a number of rolls equal to the Potency of the hex, or until the Duration expires. With +2 Reach, this can include spellcasting rolls and the use of other supernatural powers.
\item Impose a number of the following Tilts equal to the Potency of the hex: Arm Wrack, Blinded, Deafened, Insane, Knocked Down, Leg Wrack, Poisoned, or Sick. This lasts for the Duration of the hex unless the subject resolves the Tilt sooner.
\item Impose a number of custom Tilts equal to the Potency of the hex for the Duration of the hex. These can be built using the rules for Generic Conditions (see p. 289). The mage may also mix and match the above effects using Potency. So, a Potency 4 hex allows the mage to levy a penalty on the subject's next two actions, as well as apply the Blinded and Leg Wrack Tilts.
\end{itemize}
Duplicate penalties and Tilts from hexes aren't added together. Only the worst has any effect, although a subject may labor under a penalty to all actions in addition to a penalty to specific actions due to a Tilt. Arm Wrack, Blinded, Deafened, and Leg Wrack are exceptions to this, each of which can be applied twice for the more severe version of the Tilt (representing both arms, eyes, ears, or legs being affected).

\subsection{Boons}
Many Fate spells bestow a boon on the subject, bestowing one of the following benefits:
\begin{smallitemize}
\item Grant 9-Again quality on a number of mundane dice rolls equal to Potency for the Duration of the spell. The subject's player can choose which of his rolls are affected by this boon (declared before the dice are rolled). If used on a chance roll, the subject does not gain the 9-Again quality, but the chance die is treated as a single die instead of as a chance die. By adding +1 Reach this becomes 8-Again, instead. The boon may also affect spellcasting rolls and
other powers at the cost of +2 Reach.
\item Grant a dice bonus equal to Potency on certain actions (usually a single Skill) for a number of rolls equal to the Potency of the spell during its Duration. Multiple boons cannot grant their bonus to the same action; only the highest bonus counts. The subject's player chooses which of his rolls are affected by this boon before rolling the dice.
\item Grant a number of beneficial Conditions (such as Charmed, Informed, Inspired, or Steadfast) equal to Potency for the Duration of the spell. None of these Conditions grant Beats when the subject resolves them (unless the caster achieved an exceptional success in the casting). As with hexes, Potency can be used to mix and match the above effects, boons that affect the same Skill do not combine their effects (only the highest bonus counts), and no character can have multiple instances of a Condition unless they apply to two different and specific things.
\end{smallitemize}

\spell{Interconnections}
{Unveiling}
{Potency}
{}
{Composure}
{This spell reveals the marks of Fate on people, places, and things the mage observes (up to one subject per turn). In addition to allowing the mage to detect any sympathetic connections between the subjects, the mage can also identify those who have violated an oath or geas, and the presence of spells with conditional Durations (see p. 192). 
\reach[1]{The mage can also detect possession, supernatural mind control, and alterations of destiny.}
\reach[2]{The mage can also discern specific information about a subject's destiny - such as the Doom of a subject with the Destiny Merit or the conditions necessary to trigger (or terminate) a spell with a conditional trigger or Duration.}
}

\spell{Oaths Fulfilled}
{Knowing}
{Duration}
{}
{}
{In folktales, witches always seem to know when their subjects fulfill (or violate) the terms of an agreement. This spell notifies the mage when a specified fate befalls its subject - whether the subject is the victim or the actor. This triggering event must be something the mage could perceive if he were present (e.g. the subject suffers an injury, goes to the restroom, breaks her word, speaks the mage's name, etc.).
\reach[1]{The mage receives a brief vision of the subject when the oath is fulfilled.}
\reach[1]{The mage can track the subject until the spell's Duration elapses. This doesn't provide knowledge of the subject's location, only of a path to the subject that is sure and swift. }
\reach[1]{The trigger event may be something the mage could only perceive using Mage Sight.}
}

\spell{Quantum Flux}
{Compelling}
{Duration}
{}
{}
{The mage reads probability and compensates for deleterious factors, attracting small turns of good fortune to negate unfortunate obstacles that stand in her way. This negates penalties to any of the subject's actions equal to Potency for a number of actions during the Duration equal to Potency.
Additionally, the subject can spend a turn during the spell's Duration aiming an action. The subject loses any Defense and must remain still while aiming. A turn spent aiming grants a bonus to the next action equal to Potency. This bonus can only be applied to mundane instant actions; extended actions and
spellcasting rolls do not benefit from it.
}

\spell{Reading the Outmost Eddies}
{Compelling}
{Potency}
{}
{Composure}
{This spell bestows a small blessing or curse that attracts good or bad fortune to its subject. Choose one:
\begin{smallitemize}
\item As a blessing, the subject achieves an exceptional success on three successes instead of five. This affects a number of rolls equal to Potency, chosen before the dice have been rolled.
\item As a curse, it eliminates 10-Again on all the subject's actions for a number of rolls equal to the Potency of the spell. 
\item A small, beneficial (or deleterious) twist of fate to befall the subject within the next 24 hours - such as finding \$20 or dropping his wallet in a puddle. The mage can exert limited control over the nature of the fortune (or misfortune), but ultimately fate decides the detail. Hostile applications of this spell are Withstood by Composure. 
\end{smallitemize}\fixEmptyNewline{}
\reach[1]{As the third effect, but the twist of fate occurs within one hour.}
}

\spell{Serendipity}
{Knowing}
{Potency}
{}
{}
{This spell grants the mage a momentary glimpse of all the potential roads her destiny may follow to her desired destination, which allows the mage to identify the next step she must take to accomplish a stated objective. Upon casting, the mage receives a clear omen that suggests a course of action that will lead her closer to her goal. This seldom guarantees immediate success,
especially if the task before her is complicated, but can provide an important breakthrough.
\reach[1]{When the mage's player makes a roll to achieve her stated goal she may substitute any Skill of the same type (Mental, Physical, or Social) as the one that the task calls for. Through some twist of fate Streetwise turns out to be just as useful in a specific social situation as Empathy, for example. This affects a number of rolls no greater than the Potency of the spell. She may use this a maximum number of times equal to the Potency of the spell.}
\reach[2]{As above, but the mage's player may substitute any Skill. The mage's street sense (Streetwise) so impresses a punkat-heart librarian that she helpfully answers all his questions about Greek history (Academics), for example.}
}

\spell{Exceptional Luck}
{Ruling}
{Potency}
{}
{Composure}
{The mage blesses the subject's endeavors or curses them with misfortune. Whether good or bad, the subject's luck is truly exceptional. This spell bestows a boon or inflicts a hex on the subject (see p. 134). The subject may Withstand a hex with Composure.
\reach[2]{The boon or hex can affect spellcasting rolls.}
\reach[2]{Spend a point of Mana. The mage can cast this spell as a reflexive action.}
}

\spell{Fabricate Fortune}
{Veiling}
{Duration}
{}
{}
{Sometimes a mage wishes to hide a child of destiny from those who would abuse her gift. Other times he wishes to convince observers that a subject has a fate that she does not. This spell conceals or falsifies fates and Destiny.
It can be used to "trick" conditional Durations or spells with conditional triggers into ignoring an event that meets the defined condition or into acting as though the stipulated event has come to pass. It can create false omens regarding the subject when she is scrutinized by Fate magic. All such deceptions provoke a Clash of Wills against those attempting to overcome
its protections.
}

\spell{Fools Rush In}
{Ruling}
{Duration}
{}
{}
{According to old wisdom Fate favors children and fools, and this spell makes the old adage true. So long as the subject has little or no detailed knowledge about a situation before he enters it, the spell allows him to act with perfect grace and timing. A turn or two of studying the scene before acting is acceptable, but extensive reconnaissance or a detailed briefing does not
permit the necessary degree of randomness this spell requires. The subject does not suffer untrained penalties during the spell's Duration. If entering an unfamiliar social situation, the subject's impression level also improves by one.
\reach[1]{The subject receives a dice bonus equal to Potency on a number of dice rolls (not including spellcasting rolls) equal to Potency during the Duration. The subject's player chooses which rolls are affected (before the dice are rolled), and they can include any action or task, so long as they are instant or reflexive actions taken "in the moment" and not arranged ahead of time.}
\reach[3]{As the first Reach effect, but this bonus can also affect spellcasting rolls.}
}

\spell{Lucky Number}
{Ruling}
{Duration}
{}
{}
{The probability of correctly guessing a phone number, a password, or lock combination on the first try is minute but not impossible. This spell allows the mage to do just that simply by entering data into an appropriate device (a password field, a telephone, a safe combination, etc.). In addition to any story
benefits, the mage gains the Informed Condition on next relevant roll that  benefits from knowledge gained through this spell. This spell uses the input device as its subject, and the mage concentrates on what she is attempting to accomplish. It does not require sympathy even for applications such as guessing the phone number for a particular person; the magic applies to the probabilities of random input, rather than locating a target. Although it will
call the nearest available phone to the person the mage is trying to reach, the spell doesn't tell the caster where that phone \textit{is}.
}

\spell{Shifting the Odds}
{Ruling}
{Duration}
{}
{}
{An Apprentice of Fate always has access to what she needs at the moment. The mage focuses on locating a particular kind of person, place, or thing, and this spell directs her steps to it unerringly as soon as possible within the next 24 hours (spell has to be active). Casting the spell looking for a kind of person in a crowd or an item anywhere it could appear is usually enough to immediately succeed. The spell can find someone with a specific Trait, occupation, or context-specific quality (e.g. "corrupt cop"), but it only locates the nearest or most available subject matching the description the mage provides, never a specific person or object (although destiny sometimes draws familiar faces together). Alternatively, the mage gains temporary access to certain Social Merits (Ally, Contacts, Mentor, Resources, or Retainer) with a rating no greater than the Potency. Fate guides her to dropped cash, unattended mundane items, or useful strangers she can easily convince to do her a quick favor. The mage may benefit from this Merit a number of times no greater than Potency, after which the money runs out or the ally of convenience goes his own way unless the mage's character spends Experiences to purchase the Merit. 
\reach[1]{The mage locates the desired object within one hour.}
}

\spell{Warding Gesture}
{Shielding}
{Duration}
{}
{}
{The mage creates a ward protecting the subject against supernatural effects that manipulate her fate - a geas, a supernatural compulsion to act against her will, or having her fate manipulated by Fate magic or similar supernatural effects. Each attempt to change the subject's destiny provokes a Clash of Wills with the mage. This spell has no effect on pre-existing alterations to the subject's destiny. Additionally, the mage may selectively exclude the subject
from any area-effect spell he casts. If cast on multiple subjects, this spell allows the mage to exclude each valid subject on a case-by-case basis.
\reach[1]{The mage can selectively exclude the subject from any spell she casts or any Attainment she uses.}
\reach[2]{The mage may selectively grant protection from supernatural effects that target an area instead of individuals. This provokes a Clash of Wills to exclude the subject from the effect, which the mage may automatically pass if protecting subjects from his own spells.}
}

\spell{Grave Misfortune}
{Fraying}
{Potency}
{}
{Composure}
{This spell attracts misfortune to the subject or makes an already injurious situation considerably worse. The next time the subject suffers at least one point of damage during this spell's Duration, increase the damage he suffers by the spell's Potency. A glancing blow instead crushes a bone, for example. The damage type is the same as that of the original source of harm. This affects a maximum number of attacks equal to Potency during the spell's Duration.
}

\spell{Monkey's Paw}
{Weaving}
{Potency}
{}
{}
{The mage interacts with a lifeless object, bringing fortune to bear on it and making it a tool of destiny. The mage either blesses or curses the object.
The object's equipment bonus is increased or decreased by the spell's Potency, which may cause it to become a dice penalty if moved below zero. The spell may not cause the bonus or penalty to exceed five dice.
\reach[1]{If the object is blessed, anyone who carries, touches, or uses it also receives the benefits of a boon (see p. 134). If the object is cursed, this user suffers a hex (see p. 134).}
\reach[1]{By spending a point of Mana, the bonus or penalty may exceed five dice.}
}

\spell{Shared Fate}
{Weaving}
{Potency}
{}
{Composure}
{Fate is an instrument of both justice and punishment. This spell braids the fates of two subjects together. Whatever befalls one subject affects the other.
Whenever one subject suffers damage, a Tilt, or an unwanted Condition, any others suffer it as well. If Scale is not increased when casting this spell, the mage herself is treated as a subject. 
\reach[1]{The link instead only works one way for one of the subjects - such that the subject suffers harm inflicted on the others but not the reverse.}
\reach[2]{The subject is not linked to the mage or any other specific subject. Instead, she suffers any damage, Tilt, or unwanted Condition she inflicts, regardless of whom she harms.}
}

\spell{Superlative Luck}
{Perfecting}
{Duration}
{1 Mana}
{}
{The mage can ensure success at virtually any task he sets out to accomplish.
The subject gains the rote quality on a number of mundane dice rolls equal to Potency. The subject's player can choose which of his rolls are affected (declared before the dice are rolled). 
\reach[2]{The subject's player may apply the spell's effects to ritual spellcasting rolls, which doubles the Gnosis-derived casting time of those spells.}
}

\spell{Sworn Oaths}
{Perfecting}
{Duration}
{}
{}
{The mage can witness a sworn oath and ensure that Fate itself enforces the subject's adherence to her vow. The subject makes a promise and states the consequences for herself if she violates the agreement. No one can be forced to take such an oath, although a subject may be placed under oath unwittingly if he voluntarily makes a vow and verbally agrees to a specified consequence, even if he doesn't realize the mage can enforce the oath supernaturally.
So long as the subject adheres to the oath she receives a boon (see p. 134). If a supernatural power would force the subject to violate her oath - either by action or inaction - the mage may make a Clash of Wills against the effect. If the subject breaks the oath (whether intentionally or not), she suffers the hex (see p. 134) she agreed to at the time she took the oath. A subject who declares "I will guard your secrets or may I be struck blind," will suffer the Blinded Tilt for the remaining Duration of the spell if he fails to guard those
secrets, for example. Once the subject has broken an oath, further violations of its terms do not levy additional hexes. If cast on multiple subjects, each subject may swear his own oath; this is often used to create contracts between two or more parties. 
\reach[1]{As long as she maintains spell control, the mage is aware whether the spell remains a boon or has shifted to being a hex.}
}

\spell{Atonement }
{Unraveling}
{Potency}
{}
{Countered effect's Potency}
{A powerful Witch can aid a hero who labors under a curse, but her remedies often demand strange rituals or arduous quests. This spell can dispel a supernatural effect enforced by the dictates of destiny, including Awakened spells. With a successful casting, the mage learns of a task chosen by fate (and the Storyteller) that will end the curse's effects on the subject forever:
\begin{smallitemize}
\item Minor curses (1-2 dots) require a minor quest (bathing in a nearby river, donating a small sum to charity, etc.).
\item Moderate curses (3-4 dots) require a medial quest (reading a rare book available in another city's library, removing all the graffiti in a neighborhood, etc.).
\item Major curses (5+ dots) demand major quests (undertaking a months-long pilgrimage, recovering an artifact that has been missing for centuries, etc.).
\end{smallitemize}
Especially powerful curses, such as those levied by ephemeral entities of Rank 6 or more, will often demand more elaborate tasks to break.
\reach[1]{The quest can be undertaken on the subject's behalf by another person who wishes to champion her. If the subject is unwilling, this spell is Withstood with Resolve.}
}

\spell{Chaos Mastery}
{Patterning}
{Potency}
{}
{}
{The mage can Pattern Fate to manipulate complex probabilities within the spell's subject or area of effect. This spell allows the mage to dictate any physically possible outcome within the bounds of the spell's subject, no matter how unlikely. The spell cannot create supernatural effects, but within the bounds of improbability the mage can cause a number of effects equal to Potency, such as:
\begin{smallitemize}
\item Create a narrative effect such as controlling how vehicles behave in a multiple-car crash..
\item By directing once-random biochemical changes within a subject, cause seizures, hallucinations, and physical events, imposing suitable Conditions such as Blind or Disabled.
\item The mage reduces a subject's next action to a chance die.
\item Attack a subject by directing chance around them, or protect a subject from
dangerous circumstances; this is not a direct-attack spell, and should use any rules for the hazard.
\end{smallitemize}
}

\spell{Divine Intervention}
{Patterning}
{Potency}
{}
{Resolve}
{The mage casts a powerful curse that either encourages the subject to achieve a goal specified by the mage when he casts this spell, or that thwarts the subject's every attempt to pursue such a goal. The subject must, however, be aware of the goal, and the mage cannot levy impossible tasks. One of the subject's Aspirations is replaced by the goal. As a goad, the subject suffers ill luck except when taking constructive action to bring her closer to achieving the stated goal. If the subject has not pursued the spell's objective in a meaningful way within the last 24 hours, she suffers a hex (see p. 134).
As a ban, the subject suffers ill luck whenever he attempts to achieve the forbidden goal. For the Duration of the spell, the subject suffers a hex if she actively strives toward it.
}

\spell{Strings of Fate}
{Patterning}
{Duration}
{}
{Resolve}
{The roads of destiny fork and converge, governing the probability of events. An adept of Fate can re-weave the strings of Fate on a subject, encouraging (if not ensuring) that a specified event will happen. The mage specifies an event that she wishes to happen to the subject. If the event would be possible without magic or any effort on the subject's part, it occurs as soon as circumstances allow as long as the spell's Duration is in effect. If the event requires the subject's participation or cannot take place without a change in
circumstances, the spell introduces opportunities to work towards the event, at least once per week while the spell remains on the subject. If the event is simply impossible, the spell has no effect. For example, if a mage casts the spell on herself and specifies that she will meet with her mentor while they are both in the same city, they will "randomly" cross paths at first opportunity.
If she casts it on a Sleepwalker ally and specifies that he will recover a stolen artifact (when, unknown to her, it has been moved) he will find travel tickets for the artifact's new location, clues pointing there, or reasons to travel there. If she cast it on a student and specifies that he will become a doctor, circumstances will hint at a transfer to pre-med. The spell cannot deal damage directly, though it can put subjects in harm's way. For example, a mage could curse a victim with this spell specifying that she will be in a car wreck, or exposed to a disease.
\reach[1]{The spell presents opportunities to work toward the specified event once per day.}
}

\spell{Sever Oaths}
{Unraveling}
{Duration}
{}
{Composure}
{To an Adept of Fate, all fetters on a being's free will are ultimately breakable, and oaths can be renegotiated. The mage may apply a number of the following effects equal to Potency:
\begin{smallitemize}
\item Free a bound ephemeral entity, or soul.
\item Change the effects of an active boon or hex.
\item Modify or negate an oath or other supernatural agreement reinforced by the dictates of Fate.
\item Change or dispel a conditional trigger.
\item Modify the Doom of a subject with the Destiny Merit.
\end{smallitemize}\fixEmptyNewline{}
\reach[2]{The spell's effects are Lasting.}
}

\spell{Forge Destiny}
{Making}
{Duration}
{}
{Composure}
{If a Master of Fate does not have a hero of destiny handy, he can simply make one. He has several means of accomplishing this at his disposal, and may apply one of the following effects: 
\begin{smallitemize}
\item The mage grants the subject a Supernatural Merit for which she qualifies with a maximum rating equal to the spell's Potency. Sanctity of Merits (see p. 99) doesn't apply to the loss of this Merit.
\item The mage increases or decreases the subject's rating in a Supernatural Merit by dots equal to Potency.
\item The mage imposes Aspirations and Obsessions on the subject equal to Potency, replacing ones chosen by the Storyteller.
\item The mage chooses the subject's Doom (p. 100). This can affect subjects who don't have the Destiny Merit.
\end{smallitemize}
}

\spell{Pariah}
{Unmaking}
{Potency}
{}
{Composure}
{One of the most terrifying curses in the arsenal of a Master of Fate, this spell turns the world against the victim:
\begin{smallitemize}
\item Anyone who encounters the subject instinctively feels uncomfortable around him, intuitively sensing the curse. Most people treat him indifferently at best - if not with open hostility. If the subject is using the Social maneuvering system (see p. 215), the impression level drops by one (from
good to average, for instance). If not, the subject takes a penalty equal to the Potency to any Social actions to persuade others to aid him or treat him with kindness.
\item Any actions aimed at helping the subject or offering aid suffer the effects of a hex (see p. 134). This includes everything from an attempt to lift the curse to providing the subject with directions to the nearest bus stop.
\item Any actions aimed at harming the subject enjoy the effects of a boon (see p. 134). This includes all forms of harm - from outright murder, to theft, to lies that cause the subject inconvenience. The caster must account for any Reach involved in the boon and hex separately.
\end{smallitemize}\fixEmptyNewline{}
\reach[1]{The mage can adjust the sensitivity of the curse. She could bless only attempts to injure the subject but not to murder him, for example.}
}

\spell{Miracle}
{Making}
{Potency}
{}
{}
{This spell causes events to unfold according to the mage's dictates. The mage receives a number of Intercessions equal to Potency, which she may use as a reflexive action during the spell's Duration. Spending one Intercession can achieve the following, affecting a single subject within sensory range:
\begin{smallitemize}
\item Increase the number of successes on a roll by one after the dice are rolled.
\item Decrease the number of successes on a roll by one after the dice are rolled. If this reduces the number of successes to less than zero, a dramatic failure results.
\item Cause a reasonably likely event to happen immediately and conveniently for the mage. An old man suffers a heart attack. A car hits a pedestrian on the sidewalk.
\end{smallitemize}\fixEmptyNewline{}
\reach[1]{Spend one Intercession and one Willpower to bring about a low-probability event. Lightning hits someone standing on the roof during a storm. Doctors bring a patient back from the edge of death using an experimental medical procedure.}
\reach[2]{Spend one Intercession, one Willpower, and one Mana to achieve the incredible. This doesn't make the impossible possible, but virtually any event with a mundane, real-world precedent is fair game. A spark causes a car to explode at a gas station. A man falls out of a plane and survives without any major injuries.}
}

\spell{Swarm of Locusts}
{Making}
{Duration}
{}
{}
{The mage creates chaotic conditions: rains of frogs, swarms of locusts, unscheduled total solar eclipses, and other similarly "Fortean" occurrences. This terrifying and obviously supernatural event wreaks havoc in the area, creating Environmental Tilts of the player's choosing. Most Sleepers suffer an immediate Breaking Point when they witness this spell.}

\end{document}
