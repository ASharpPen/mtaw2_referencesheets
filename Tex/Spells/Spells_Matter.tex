\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Matter}

\section{Matter}
\textbf{Purview:} Alchemy, gases, solids, liquids, shaping, crafting, transmutation, stasis.\\
The gross Arcanum of Stygia, Matter is a study in contradictions.
It is at once the Arcanum of stasis and transformation, creating objects that will last a thousand years and reshaping objects at a whim. Matter's purview is all the inert, lifeless things of the world: iron and steel, air and water, and all the riches of the earth. Things which were once alive but are no longer, as well as substances derived from life but which are not themselves alive, are also governed by this Arcanum. A Matter spell can rot and warp the boards of a wooden house, or turn a glass of water into wine. Matter is seen as the most
base of the Arcana by many mages, closest to the profane Lie, but the Stygian Masters know that the crude Matter of the Fallen World is but an echo of the perfected material of the Supernal.

\spell{Craftsman's Eye}
{Knowing}
{Duration}
{}
{}
{Under the craftsman's eye, no tool is mysterious. By studying an object for one turn, the subject gains a complete understanding of the object's intended function. From a tool as simple as a hammer to an intricate puzzle box, the item's intended purpose is plain to see. If the object has no purpose (for example, a simple rock), the spell reveals that too. Likewise, if something prevents the object from fulfilling its purpose (for example, a car missing its spark plugs can't drive), the spell reveals the nature of the problem.
\reach[1]{The subject's senses expand to an understanding of how to use the examined object. Not only does this reveal things like the combination to a safe or the solution to a puzzle, it grants the subject 8-Again on all actions made using the studied object for its intended purpose. Only the most recently studied object gains this benefit.}
\reach[2]{As above, plus the spell reveals all potential uses of an object, fanned out in a vast array of Supernal symbols around the object. Focusing on a particular use might require a reflexive Wits + Composure roll for especially complex items.}
\conjunction{Add Fate}{1}{The mage names a particular task when casting the spell (e.g. "get leverage on Carruthers," "translate the Codex Afire"). Any object that might help with that task seem to loom larger, to be more physically present, and are immediately obvious to the subject as soon as she lays eyes on them.}
}

\spell{Detect Substance}
{Unveiling}
{Duration}
{}
{}
{The mage chooses a number of substances or objects that fall under Matter's purview equal to the spell's Potency. As long as this spell is active, the subject is automatically aware of the presence and location of the chosen substance within the area of effect. The chosen substance can be as broad or as specific
as the mage likes ("ferrous metal," "stainless steel," "a knife," and "my hunting knife" are all valid options). 
\conjunction{Add Time}{1}{The subject can detect whether the chosen substance has been in the area within an amount of time equal to the spell's Duration.}
\conjunction{Add Forces}{1}{The subject can search for specific types of electronic information, such as digital audio, photographs, or text documents. Not only will the spell reveal which devices have the chosen file type on board, if she's actually using the device the mage knows where on the device the files are stored.}
}

\spell{Discern Composition}
{Knowing}
{Potency}
{}
{}
{The subject becomes aware of the precise composition of an object: its weight and density, as well as the precise elements that make it up.
\reach[1]{The subject also becomes aware of any objects concealed within the object: a gold relic hidden in a secret compartment in a stone statue, for example.}
\reach[1]{The subject instinctively knows the object's structural weak point. Attempts to damage the object reduce its Durability by -1 per point of the spell's Potency. This benefit lasts until the object is destroyed or fully repaired.}
\conjunction{Add Space}{2}{The subject is aware not only of what the object is made of, but of precisely where the material came from (e.g. where the ore was mined, where the tree that made the board was felled, or where the circuit board was manufactured). Casting this spell on a Supernal Artifact strikes the subject with an overwhelming rush of images and symbols: Roll the Artifact's dot rating, contested by the subject's Gnosis. If the Artifact earns more successes, the subject gains the Shaken Condition. Resolving the Condition grants an Arcane Beat.}
}

\spell{Lodestone }
{Compelling}
{Duration}
{}
{}
{The mage chooses a substance or type of object. As long as the spell remains active, those objects within the spell's Area are drawn to the spell's subject: Dropped coins bounce toward her, water flows in her direction as long as she's standing downstream, and so on. Unless the object is capable of moving under its own power, this spell can only nudge the object when an external force is imparted on it: a ball might roll across the floor, but a heavy book won't fly off a table into the subject's hand. (It might, however, tip and fall off a shelf if it was precariously balanced to begin with.) Alternately, the mage can repel objects from the subject in the same fashion. This spell isn't strong enough to deflect a weapon swung or fired with intent to harm, but it can certainly keep the mage dry in a rainstorm or keep a cloud of tear gas at bay.}

\spell{Remote Control}
{Compelling}
{Potency}
{}
{}
{With the commanding power of Stygia, the subject can control any mechanical object, making it fulfill its function. She might flip a light switch, cause an industrial press to slam downward, or shift a car into gear. Anything that's within the bounds of a single instant action, and which the subject device is capable of performing, is fair game. Should the action require a Skill roll, treat the spell's Potency as its successes.
\reach[1]{The subject can perform more complex tasks while controlling the object, including extended actions or maintaining continuous control of the object as long as the spell's Duration lasts.}
}

\spell{Alchemist's Touch}
{Shielding}
{Potency}
{}
{}
{Draped in the leaden shrouds of Stygia, the subject may handle even the most dangerous of substances without fear. When the spell is cast, the mage chooses a particular form of matter: The subject is largely immune to its deleterious effects. The material cannot inflict bashing damage on her at all, and she reduces the damage from lethal sources of harm by the spell's Potency. The spell has no effect on aggravated damage. This spell only protects the mage from harm that comes due to an intrinsic property of the material. The damage from a gun or a sword, for example, comes from the force behind the impact and thus isn't reduced by this spell. However, a mage under the protection of this spell can handle radioactive or caustic substances or walk through a cloud of chlorine gas with no ill effects.
\reach[1]{The mage chooses another form of matter the spell protects against.}
\reach[2]{The mage is immune to bashing and lethal damage from the material, and reduces any aggravated damage by the spell's Potency.}
\conjunction{Add Forces}{2}{The subject is protected from extreme temperatures caused by the substance's state. She can walk across lava, scoop up a handful of molten steel without being burned, or dip a finger in liquid nitrogen without it freezing.}
}

\spell{Find the Balance}
{Ruling}
{Duration}
{}
{}
{Those initiated into the Stygian Mysteries know that understanding a tool is only the first step toward perfecting it. By subtly manipulating the density and purity of a tool, the mage improves its balance and heft. The tool grants its user the 9-Again quality for the Duration of the spell, so long as it's a tool that can benefit from balance or weight distribution.
\reach[1]{The tool grants the 8-Again quality instead.}
}

\spell{Hidden Hoard}
{Veiling}
{Duration}
{}
{}
{This spell renders Matter difficult to detect. It isn't invisibility, precisely; rather, the spell veils the subject's connection to the Supernal truths, making it seem insignificant and beneath notice. Mundane attempts to detect the subject fail automatically. Spells and powers that would detect the veiled object are subject to a Clash of Wills.
}

\spell{Machine Invisibility}
{Veiling}
{Duration}
{}
{}
{By means of this spell, the mage blinds the eyes and ears of inert matter to her subject's presence: cameras refuse to see her, microphones refuse to hear her voice, and so on. Supernatural objects (such as remote-viewing Artifacts or perhaps a ghost-haunted camera) provoke a Clash of Wills.
\reach[1]{This spell also applies to unliving constructs animated with magic, including zombies and golems. Such beings always provoke a Clash of Wills.}
}

\spell{Shaping}
{Ruling}
{Potency}
{}
{Durability}
{Liquids, gases, and amorphous solids are the mage's playthings with this spell. She can shape them into any form she desires, manipulating them in defiance of gravity, for as long as the spell lasts. This spell cannot change the state of matter (e.g. from solid to liquid), but substances that have been temporarily transformed into shapeable states by magic may be affected. Particularly intricate shapes may require a reflexive Wits + Crafts roll, at the Storyteller's discretion.
\reach[1]{The mage can alter the shape of solid substances as well. If used to warp a tool or weapon, each point of Potency reduces the subject's equipment bonus or damage by -1. If the equipment bonus or damage is reduced to 0, the object is useless.}
\reach[1]{If the mage is creating or repairing an object using this spell, reduce the number of successes required on the extended action by one per point of Potency. This can't reduce the number of required successes below one.}
\reach[2]{The mage can create an appropriate Environmental Tilt, such as Earthquake, Flooded, or Howling Winds.}
}

\spell{Aegis}
{Weaving}
{Duration}
{}
{}
{By adjusting the properties of matter, the mage may make silk shirts bullet-proof, or tear through bulky riot suits with her bare hands. The spell is cast upon a wearable object (giving living beings Armor is a function of Life). For each level of Potency, the player chooses one of the following effects:
\begin{smallitemize}
\item Raise or lower ballistic Armor rating by 1
\item Raise or lower general Armor rating by 1
\item Raise or lower Defense penalty by 1
\end{smallitemize}\fixEmptyNewline{}
\reach[1]{The armor becomes immune to the Armor-Piercing effect.}
}

\spell{Alter Conductivity}
{Weaving}
{Potency}
{}
{}
{With this spell, the mage alters a subject's base properties, changing the manner in which it conducts electricity. This spell can automatically shut down any electrical device whose power isn't great enough to inflict damage, or it can increase or decrease the amount of electricity that can flow through the object. For each level of Potency, the spell allows the object to conduct two points worth of electrical damage, or reduces electrical damage by two. The object must still be in contact with an appropriate source of electricity to deal this damage; even a Potency 6 spell won't let the power from a household wall outlet inflict more than four points of bashing damage (see Electricity on p. 224). Reducing electrical damage to zero also shuts electrical devices down - for example, completely snuffing a subway rail's conductivity shuts the trains down.
\reach[1]{The mage can alter the subject's ability to transmit other forms of energy, such as heat, sound, or even light. Each additional form of energy is an extra Reach.}
}

\spell{Alter Integrity}
{Perfecting}
{Potency}
{}
{Durability}
{By rotating an object's resonance into or out of alignment with Stygian truths, the mage can strengthen or weaken its material. Every level of Potency either increases or decreases the object's Durability by 1. This does not increase the object's Structure, but see below.
\reach[1]{In lieu of increasing Durability, one level of Potency may be "spent" to give the object 2 additional points of Structure. If the spell wears off and the object has taken more damage than it has Structure, it crumbles to dust.}
\reach[2]{The effect is Lasting.}
}

\spell{Crucible }
{Perfecting}
{Potency}
{}
{}
{With this spell, an object takes on a glimmer of Supernal purity. If its primary purpose is as a tool, it grants 8-Again on a number of rolls equal to the spell's Potency. Valuable objects, such as gold or diamonds, become incredibly pure and beautiful. Add the spell's Potency to the object's Availability rating to determine its increased value. This spell cannot increase an object's Availability to more than twice its original rating.
\reach[1]{For one point of Mana, the object grants the rote action quality on a number of rolls equal to the spell's Potency. As long as the spell's Duration lasts, its wielder may spend one point of Mana at any time to "recharge" this effect, granting the rote action quality on an additional number of rolls equal to the spell's Potency.}
\reach[1]{The spell may increase the object's Availability to three times its original rating.}
}

\spell{Nigredo and Albedo}
{Perfraying}
{Potency}
{}
{}
{All matter contains within itself the Supernal Truth of its own perfection - or its annihilation. This spell allows the mage to repair or destroy objects, restoring lost Structure or inflicting damage equal to the spell's Potency.
\reach[1]{When inflicting damage, ignore the object's Durability.}
}

\spell{Shrink and Grow}
{Weaving}
{Potency}
{}
{Durability}
{By means of this spell, the mage may bring an object's Supernal reflection closer to the world or push it farther away. This in turn causes the Supernal to cast a larger or smaller shadow into the Fallen World, effectively making the object grow or shrink. Each level of Potency either adds or subtracts one from the subject's Size. Size 0 objects can be shrunk down to roughly the size of a dime.
\conjunction{Add Life}{3}{The spell can affect living subjects. Unwilling subjects may Withstand with Stamina.}
}

\spell{State Change}
{Weaving}
{Duration}
{}
{Durability}
{The mage can transmute any inorganic material one "step" along the path from solid to liquid to gas. This magically-induced state change does not change the material's temperature: liquefied steel remains as cool to the touch as if it were solid, and vaporized ice is still freezing cold. Transforming a liquid or gas into a solid gives the new object a Durability equal to the spell's Potency; Structure is determined as Durability + Size. When the Duration wears off, the substance returns to its natural state, but keeps the form it held during its altered state. (In the case of materials turned to gas, this often means a fine rain or snow of unusual composition.)
\conjunction{Add Forces}{3}{The mage may transmute matter into plasma.}
\reach[1]{The mage may transform a solid directly into a gas, or a gas directly into a solid.}
}

\spell{Windstrike}
{Weaving}
{Potency}
{}
{}
{The very air (or other fluid matter) strikes out against the mage's enemies. The wind buffets and strikes like a fist, or water lashes out like a whip. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts bashing damage.
\reach[1]{The warped matter of this spell sticks around after casting, creating an Environmental Tilt like Flooded or Heavy Winds in the immediate vicinity.}
}

\spell{Wonderful Machine}
{Weaving}
{Potency}
{}
{}
{This spell allows a mage to swiftly superimpose pieces of various objects into one another in such a way as to produce a desired result. With this spell, a mage could, for example, integrate a nail-gun and shotgun together to produce a weapon that fires a barrage of nails with each pull of the trigger. For each level of Potency, the mage may transpose one quality (such as a rotisserie's generation of heat or its ability to rotate another object within it) from a given mechanical object onto another mechanical object. In the case of combining firearms with other firearms, one weapon characteristic can be swapped out for another (creating a pistol, for example, that uses shotgun shells for ammunition). A firearm can also be incorporated fully into another device, effectively disguising the weapon until it is first used (or it undergoes close mystic or mundane inspection).
\conjunction{Add Life}{3}{The machine properties can be grafted onto a living thing, or vice versa. The mage might, for example, make a bird that can breathe fire or a butane torch that can fly.}
}

\spell{Ghostwall}
{Patterning}
{Duration}
{}
{}
{All Fallen Matter is merely a shadow of Supernal truth, and this spell reveals the truth of that axiom. The mage renders a volume of inert matter wholly or partly insubstantial, no more "real" than an illusion. Insubstantial objects remain where they were when transfigured (that is, they don't fall to the center of the Earth or fly off into space). Objects made insubstantial by this spell aren't in Twilight, they simply don't register as "real."
\conjunction{Add Death or Mind or Spirit}{3}{The insubstantial object may be shifted into Twilight, attuned to the used Arcanum.}
}

\spell{Golem}
{Patterning}
{Potency}
{}
{}
{This spell animates a statue or other object, allowing it to move and act almost as if it were alive. Each level of Potency effectively grants the mage a dot of the Retainer Merit. The Golem's "field" includes simple physical labor, combat, and other uncomplicated tasks. The golem is completely mindless, and can only execute whatever order the caster gave it last. Orders must be very simple. If attacked, the golem has no Defense, but has Durability appropriate to its makeup (see Objects on p. 223) and Structure equal to Durability + Size.
\conjunction{Add Death or Spirit}{4}{Bind a ghost or spirit into the golem to serve as an animating intelligence. The golem still uses its Retainer rating to determine dice pools, but the ephemeral being can use any of its powers, and the golem's "field" is whatever the entity is capable of.}
\conjunction{Add Mind}{5}{Create an intelligence from nothing that will guide the golem and inform its "field." See "Psychic Genesis" on p. 159 for creating a mind from scratch.}
}

\spell{Piercing Earth}
{Patterning}
{Potency}
{}
{}
{Much like Windstrike (see p. 157), this spell causes inanimate matter to lash out at the subject. But where Windstrike lashes out with air and water, this spell causes the Earth itself to rise up and crush the subject. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts lethal damage. 
\reach[1]{The warped matter of this spell sticks around after casting, creating an Environmental Tilt like Earthquake in the immediate vicinity.}
\reach[1]{For one point of Mana, the spell inflicts aggravated damage.}
}

\spell{Transubstantiation }
{Patterning}
{Duration}
{}
{}
{The mage can transmute any inert matter into any other form of inert matter: lead into gold, water into wine, wood into chlorine gas, etc. The purity and quality of the transmuted matter is determined by the spell's Potency: treat Potency as an equipment bonus or equivalent Resource Merit dots for a single purchase, whichever is appropriate. Both the initial substance and the transubstantiated substance must be relatively pure: Wood can be transformed into gold, but not into gold chased with silver. (The Stygian Mysteries teach that "purity" is a perceptual concept - so, for example, even though "wine" and "steel" are made up of numerous compounds, they are concrete enough as concepts to be transmuted).
\conjunction{Add Life}{4}{Transform matter into living things, or transform a living being into inert matter (but see p.127 for rules on permanently transforming someone with magic). Unless the mage also adds Mind $\spelldots{5}$, any organism created is mindless, driven purely by instinct. }
\reach[1]{The mage may transmute multiple substances into a single substance or vice versa.}
}

\spell{Annihilate Matter}
{Unmaking}
{Potency}
{}
{Durability}
{The mage can destroy inert matter, reducing it to nothingness and utterly dissolving its atomic lattice. In effect, he makes it cease to be. Whereas objects destroyed by Nigredo and Albedo (see p. 157) shatter or crumble as appropriate, matter destroyed by this spell is annihilated; nothing remains of it. Magical objects and materials, such as tass or Artifacts, cannot normally be destroyed with this spell. 
\reach[1]{For one point of Mana, the spell can destroy magical objects as well. In this case, the spell is Withstood by the Supernatural Tolerance of the subject or its maker, as appropriate (e.g. the Rank of the spirit bound to a fetish, the Blood Potency of a vampire whose Vitae the mage is trying to destroy, or the Gnosis of the mage who crafted the object). If no such trait applies, use the object's Durability.}
}

\spell{Ex Nihilo}
{Making}
{Potency}
{}
{}
{The mage creates an object out of nothing. The object may be any simple tool or relatively uncomplicated machine (a revolver qualifies, but an automatic handgun is too complex). The object's size is determined by the Scale factor. The spell's Potency may be allocated as the mage wishes between Durability or equipment bonus.
\reach[1]{Create a complex machine or electronic device, like a car or a smartphone. The device must still be something that operates according to known physical principles: no teleporters or warp drives.}
}

\spell{Self-Repairing Machine}
{Making}
{Potency}
{}
{}
{This spell imbues an object with a small semblance of life - specifically, the ability to repair itself. As long as the spell lasts, the object heals (Potency) Structure every day. 
\reach[1]{The machine heals (Potency) Structure every hour.}
\reach[2]{The machine heals (Potency) Structure every 15 minutes.}
}

\end{document}