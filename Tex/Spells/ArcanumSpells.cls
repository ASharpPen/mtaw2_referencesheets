\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ArcanumSpells}

\LoadClass[a4paper,10pt,twocolumn]{article}

\RequirePackage[margin=0.5in]{geometry}
\RequirePackage{hyperref}
\RequirePackage[open]{bookmark}
\RequirePackage{ifthen}
\RequirePackage{xstring}
\RequirePackage{needspace}



\setcounter{secnumdepth}{0}

%\providecommand{\arcanum}{Life}

\def\arcanum{Arcanum}
\providecommand{\setArcanum}[1]{\def\arcanum{#1}}

\providecommand{\practiceCompelling}{1}
\providecommand{\practiceKnowing}{1}
\providecommand{\practiceUnveiling}{1}
\providecommand{\practiceRuling}{2}
\providecommand{\practiceShielding}{2}
\providecommand{\practiceVeiling}{2}
\providecommand{\practiceFraying}{3}
\providecommand{\practicePerfecting}{3}
\providecommand{\practicePerfraying}{3}
\providecommand{\practiceWeaving}{3}
\providecommand{\practicePatterning}{4}
\providecommand{\practiceUnraveling}{4}
\providecommand{\practiceMaking}{5}
\providecommand{\practiceUnmaking}{5}

\providecommand{\pracToNumber}[1]{\csname practice#1\endcsname}

\providecommand{\col}{\vfill\break}

\newcount{\dotcounter}
\providecommand{\spelldots}[1]{
	\dotcounter=0
	\loop\ifnum\dotcounter < #1
	\bullet
	\advance\dotcounter by 1
	\repeat
}

\providecommand{\spell}[6]
	{\needspace{10\baselineskip} \subsection{#1 (\arcanum{} \texorpdfstring{$\spelldots{\pracToNumber{#2}}$}{\pracToNumber{#2}})}
	\textbf{Practice:} #2\\
	\textbf{Primary Factor:} #3\\
	\ifthenelse{\equal{#4}{}}{}{\textbf{Cost: } #4\\}
	\ifthenelse{\equal{#5}{}}{}{\textbf{Withstand:} #5\\}
	#6
}

%\spell[SpellName]
%{Practice}
%{Primary Factor}
%{Cost}
%{Withstand}
%{Description}

\providecommand{\reach}[2][]{\newline\textbf{+#1 Reach:} #2}
%\reach[nr of reaches]{Description}

\providecommand{\conjunction}[3]{\newline\textbf{#1 $\spelldots{#2}$:} #3}
%\conjunction{Description}{Nr of spell dots required}

\providecommand{\soption}[2]{\\ \textit{#1}: #2}
%\soption{Option name}{Description}

%For when newlines are done by commands, but there isn't a line to skip. E.g. itemize does this.
\newcommand{\fixEmptyNewline}{~\vspace{-\baselineskip}}

\newenvironment{smallitemize}
{ \begin{itemize}
	\setlength{\itemsep}{0pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}     
    }
{\end{itemize}}