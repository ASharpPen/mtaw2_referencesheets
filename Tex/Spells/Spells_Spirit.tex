\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Spirit}

\section{Spirit}
\textbf{Purview:} Essence, spirits, the Shadow, the Gauntlet.\\
The subtle Arcanum of the Primal Wilds deals with repercussions: What we do in this world has echoes that we cannot see or hear or feel, but which are no less real for all that. Spirit mages understand those repercussions, and know that it falls to them to act as intercessors between the Visible and the Invisible. Spirit's purview is the Shadow Realm and its denizens, the spirits. The flows of Essence that empower them and the Gauntlet that holds them apart from our world also fall under this Arcanum's dominion.

\subsection{The Gauntlet}
The membrane between worlds isn't a simple barrier, but a medium of its own that those crossing between material and Shadow have to push through. The Gauntlet's Strength or thickness depends on a wide range of factors in both worlds, not all of which a mage can know. The Gauntlet is generally strengthened by the presence of humans. The tumult of human emotion and activity keeps the worlds of Flesh and Spirit apart, and simultaneously generates new spirits and the Essence they feed on. Any spell cast on a subject on the other side of the Gauntlet or on the Gauntlet itself is Withstood by the Gauntlet Strength. If the Gauntlet Strength at the caster's location and the Gauntlet Strength at the subject's location are different, apply the lower value (the mage can either direct the Supernal energies to cross the Gauntlet and then seek out the subject, or vice versa). If a mage casts on a subject she can see across the Gauntlet in real time (e.g. with the Reach effect of the Exorcist's Eye spell), it counts as viewing that subject remotely.

\subsection{Gauntlet Strength}
Gauntlet Strength is both a number and a dice modifier. Mages mainly use the strength number, while some mechanics use the modifier, such as spirits using the Reaching Manifestation to use their powers across the Gauntlet. The following chart isn't a definitive guide to Gauntlet Strength; areas of high or low activity can create thin or thick spots - a deserted graveyard in an inner-city borough, for example, may have a lower Gauntlet than the surrounding blocks.

\begin{table}[!ht]
\tiny
\begin{tabular}{l l l}
	\textbf{Location Strength} & \textbf{Dice} & \textbf{Modifier} \\
	Dense urban areas & 5 & -3 \\
	City suburbs, towns & 4 & -2 \\
	Small towns, villages & 3 & -1 \\
	Wilderness, countryside & 2 & 0 \\
	Locus & 1 & +2 \\
	Verge & 0 & n/a \\
\end{tabular}
\end{table}

\spell{Coaxing the Spirits}
{Compelling}
{Potency}
{}
{Composure or Rank}
{Though most spirits are but slumbering motes that have no will or sapience, the mage may coax them to brief activity in accordance with their natures. She may compel the spirit (or its physical representation) to take a single instant action in accordance with its nature. A frightened animal might attack or flee, a car might start up, or a cliff face might start a small avalanche. The spell is Withstood by the Rank of the spirit coaxed or the Composure of a living representation, whichever is higher. 
}

\spell{Exorcist's Eye}
{Unveiling}
{Duration}
{}
{}
{The first spell most Spirit mages learn, this spell allows the mage to perceive and speak with spirits in the physical world, whether they are roaming freely in Twilight, slumbering within an object (including discorporated spirits in hibernation), or possessing a living being. She can also sense any spirit-related Manifestation Conditions in the area. Finally, she can see the conduit of any spirit with the Reaching Manifestation, but cannot communicate across the Gauntlet.
\reach[1]{The mage may shift her perceptions to see across the Gauntlet and into the Shadow (or into the physical world if she's in the Shadow). The spell is Withstood by the Gauntlet Strength.}
\conjunction{Add Death or Mind}{1}{These benefits extend to ghosts or Goetia, respectively.}
}

\spell{Gremlins }
{Compelling}
{Potency}
{}
{}
{Just as the spirit of an object can be coaxed to help, it may also be coaxed to hinder. When a character fails a roll using this spell's subject as equipment, the spell converts the failure into a dramatic failure. The spell converts a number of failures equal to its Potency. If the object's user is a player's character, the player gains a Beat as normal.
\reach[1]{As long as the object is within sensory range, the mage can choose which failures become dramatic failures.}
}

\spell{Invoke Bane}
{Compelling}
{Duration}
{}
{Rank}
{The spirits know what causes them pain, and avoid it at all costs. This spell forces a spirit to avoid its Bane even more assiduously than normal. The spirit must spend a point of Willpower to even come within the area of influence of its Bane (described by the Area factor of the spell), and cannot touch it at all. If the spirit is already within the proscribed area and fails the roll, it must flee immediately. This spell does not affect spirits above Rank 5.
}

\spell{Know Spirit}
{Knowing}
{Potency}
{Rank}
{}
{To command the spirits, one must first understand them. This spell allows the mage to glean a number of the following facts about a spirit equal to the Spell's Potency:
\begin{smallitemize}
\item \textit{What is the spirit's name?}
\item \textit{What is its Rank?}
\item \textit{What Manifestations does it possess?}
\item \textit{What Numina does it possess?}
\item \textit{What are its Influences, and roughly how strong are they?}
\item \textit{What is its Ban or Bane?}
\end{smallitemize}
}

\spell{Cap the Well}
{Shielding}
{Duration}
{}
{}
{Any creature becomes more pliable when its food source is controlled. This spell wards a source of Essence, making it difficult for spirits to feed from it - but not harder to sense. Any attempt by a spirit to feed on the Essence (or a mage, werewolf, or other being to siphon the Essence) provokes a Clash of Wills.
}

\spell{Channel Essence}
{Ruling}
{Potency}
{}
{}
{A wise master knows that sometimes she must reward rather than punish. This spell allows the mage to draw Essence into her Pattern from a Resonant Condition or channel Essence to a spirit or suitable receptacle. The mage may transfer an amount of Essence equal to the spell's Potency. However, she cannot channel more Essence per turn than her Gnosis-derived Mana per turn rate. Essence stored within the mage's Pattern remains even after the Duration expires; however, she can only hold a combined amount of Mana and Essence equal to her Gnosis-derived maximum Mana.
\conjunction{Add Death or Mind}{2}{The spell may be cast on a ghost or Goetia.}
\reach[1]{The mage may siphon Essence directly from a spirit, though the spirit may Withstand the spell with Rank.}
}

\spell{Command Spirit}
{Ruling}
{Potency}
{}
{Rank}
{Sometimes the gentle approach must give way to raw dominance. This spell allows the mage to command a spirit to undertake a number of actions equal to the spell's Potency. This compulsion only lasts as long as the spell's Duration, so the spirit might abandon an indefinite or extended action when the spell's Duration wears off. Commands that go against the spirit's self-interest (including abandoning a host or Fetter) provoke a Clash of Wills. This spell has no effect on spirits above Rank 5.
}

\spell{Ephemeral Shield}
{Shielding}
{Duration}
{}
{}
{To master the spirit world, one must show no vulnerability. This spell protects the subject against the Numina, Influences, and Manifestations of spirits or spiritual monsters. Such attacks must succeed at a Clash of Wills to harm the subject.
\reach[1]{Spells of the Spirit Arcanum and the spiritual magic of werewolves are likewise deflected.}
\reach[1]{The protection afforded by this spell extends to spirits' physical attacks.}
}

\spell{Gossamer Touch}
{Ruling}
{Duration}
{}
{}
{Sometimes the only way to command a spirit is with raw, brute force. This spell renders the subject's flesh solid to spirits in Twilight, allowing her to interact with them physically.
\conjunction{Add Death or Mind}{2}{These benefits extend to ghosts or Goetia, respectively.}
\reach[1]{Any object the mage carries is similarly solid to Twilight spirits.}
\reach[1]{The mage shapes her body into a powerful tool against the spirits. Her unarmed attacks against spirits count as a weapon with a damage modifier equal to the spell's Potency.}
}

\spell{Opener of the Way}
{Ruling}
{Duration}
{}
{}
{The shaman is not only intercessor, but also gatekeeper. This spell allows the mage to shift the Resonant Condition on the subject to the Open Condition, or vice versa.
}

\spell{Shadow Walk}
{Veiling}
{Duration}
{}
{}
{Sometimes the lords of shadow must walk unseen among their prey. This spell shrouds the subject from the notice of spirits and Spirit magic. Any supernatural effect that would detect her provokes a Clash of Wills.
}

\spell{Slumber}
{Ruling}
{Duration}
{}
{Rank}
{A fool exhausts herself trying to kill what cannot die; better to send hostile spirits into a deep sleep. This spell reduces the frequency with which a spirit that is hibernating after being destroyed (see p. 257) regains Essence. Instead of regaining one point of Essence per day, it regains one point of Essence every (Potency) days; but the effect still ends when the spell's Duration expires.
}

\spell{Bolster Spirit}
{Perfecting}
{Potency}
{}
{}
{The mouse who plucks the thorn is often more respected than the roaring lion. Each level of Potency of this spell heals a spirit of two boxes of bashing damage.
\reach[1]{In lieu of healing damage, the mage may expend one Potency to increase one of the spirit's Attributes by +1 for the Duration of the spell. The spirit's Rank-derived Attribute maximum still applies.}
\reach[2]{The mage may spend one Mana to increase the spirit's Rank by 1, increasing its maximum Attribute levels and Essence pool, as well as awarding it one new Numen.}
}

\spell{Erode Resonance}
{Fraying}
{Duration}
{}
{}
{Sometimes a healer must cut out diseased flesh so that the whole may heal. This spell removes the Open or Resonant condition on its target entirely. The effect is Lasting.
\reach[1]{Any future attempt to reestablish the same Resonant Condition while the spell is in effect is penalized by Potency.}
}

\spell{Howl From Beyond}
{Fraying}
{Potency}
{}
{}
{With this spell the mage calls forth a torrent of Essence from the spirit world, which buffets her foes and leaves them drained in body and soul. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts bashing damage. This spell can target physical beings or spirits in Twilight.
\reach[1]{The subject becomes more vulnerable to spiritual predation: she gains the Open condition.}
\reach[1]{The spell can target beings on the other side of the Gauntlet, but is Withstood by the Gauntlet Strength.}
}

\spell{Place of Power}
{Perfraying}
{Potency}
{}
{Gauntlet Strength}
{Even the mightiest shaman needs a place to sleep safely, and a place to do her workings where the wall between worlds is thin. This spell allows the mage to either raise or lower the local Gauntlet Strength by an amount equal to the spell's Potency within the spell's Area.
\reach[1]{The mage may alter the Gauntlet independently on either side. She might, for example, reduce the Gauntlet rating from the material side while raising it on the Shadow side.}
}

\spell{Reaching}
{Weaving}
{Duration}
{}
{Gauntlet Strength}
{The Spirit mage is a being of two worlds. With this spell, the mage may interact physically and magically interact with things on the far side of the Gauntlet, whichever realm she is in.
\reach[1]{The mage opens an Iris between the physical world and the Shadow, which anyone or anything can pass through. The Scale factor determines how big the Iris is. By adding another Reach, the mage may restrict access to the Iris by means of a Key.}
}

\spell{Rouse Spirit}
{Perfecting}
{Potency}
{}
{Rank}
{What slumbers can always be awoken. This spell can also rouse a hibernating spirit prematurely. The Potency required for this effect is the difference between the spirit's current Essence and its total Corpus. The spirit awakens immediately, with only its rightmost Corpus box cleared.
\reach[1]{For each Reach applied, the spirit wakes with one additional Corpus box cleared.}
}

\spell{Spirit Summons}
{Perfecting}
{Duration}
{}
{Rank}
{The mage sends out a call to the nearest spirit within her sensory range. Conversely she can summon spirits she knows personally. She may send a general call and the nearest spirit will answer, or she can specify the type of spirit by Resonance. The spell does not work on spirits above Rank 5.
\reach[1]{The spell also creates the Open Condition on the area, even if it does not match the spirit's Resonance.}
\reach[1]{The mage may give the spirit a single, one-word command to follow. The spirit is not compelled to complete a task if it cannot finish the command before the Duration of the spell elapses.}
\reach[1]{The mage may summon spirits from across the Gauntlet. If she is in the vicinity of an open Iris to the other side, the spell functions as normal. If not, the spell is Withstood by the greater of Rank or the Gauntlet Strength. Spirits may only cross the Gauntlet if they have the ability to do so.}
\reach[2]{The mage may give the spirit a complex command to follow. The command must be a single task, but the mage can describe the task within a sentence or two.}
}

\spell{Banishment }
{Unraveling}
{Potency}
{}
{Rank}
{This spell strips a spirit of its ability to act in the world, reminding it of its place. This spell strips a number of Manifestation Conditions from the spirit (or its host) equal to the spell's Potency. The effect is Lasting, but the spirit may use its Influences and Manifestations to reestablish the Conditions as normal. This spell does not work on spirits above Rank 5.
\conjunction{Add Death or Mind}{4}{The spell's effects extend to ghosts or Goetia.}
\reach[1]{The target cannot attempt to recreate the destroyed Conditions on the same victim or location until the spell's Duration elapses.}
}

\spell{Bind Spirit}
{Patterning}
{Duration}
{}
{Rank}
{What shows mastery more than a leash? With this spell, the mage may bind a spirit to the world, granting it a Manifestation Condition (see p. 258). The mage may grant a number of Conditions equal to the spell's Potency, and must create any prerequisite Conditions as well, if they aren't already present. The entity immediately enters the Manifestation of the mage's choice, and may not leave it while the spell remains in effect. This spell does not work on spirits above Rank 5.
\conjunction{Add Death or Mind}{4}{The spell's effects extend to ghosts or Goetia.}
}

\spell{Craft Fetish}
{Patterning}
{Duration}
{}
{Rank}
{A vanquished foe can be a useful tool. This spell allows the mage to bind a hibernating spirit into a fetish, a kind of magical item. Fetishes work like an Imbued Items, save that a fetish is powered by Essence and, instead of holding a Supernal spell, it holds one of the bound spirit's Influences and, possibly, some of its Numina. Creating a fetish requires that the spell have one Potency per dot of Influence the object will possess, plus one Potency per Numen. A fetish doesn't have to host all of the spirit's abilities. Activating the powers within the fetish is an instant action and uses the spirit's dice pool. The fetish has its slumbering spirit's Essence pool and can recharge Essence in a Resonant location just like a hibernating spirit, or it can receive Essence from another spirit or via Channel Essence (see p. 180) or similar magic. The fetish's user can pay Essence out of the fetish's pool to power its abilities. If the bound spirit ever acquires Essence equal to its Corpus, however, the spell ends immediately. The mage may also create a much simpler fetish that hosts no spirit, but can hold Essence. Such a fetish holds 10 Essence, plus a number of Essence equal to the spell's Potency. Triggering the bound spirit's Ban or Bane immediately destroys the fetish. 
}

\spell{Familiar}
{Patterning}
{Duration}
{}
{}
{The mage creates a Familiar bond between a spirit and a mage, who must both be subjects of the spell. The spirit may not be greater than Rank 2. The mage gains the Familiar Merit and the spirit the Familiar Manifestation Condition for the Duration of the spell. Both parties must be willing, and can end the bond whenever they wish.
\conjunction{Substitute Death or Mind}{4}{The mage may bind a ghost or a Goetia as a familiar instead.}
}

\spell{Shadow Scream}
{Unraveling}
{Potency}
{}
{}
{The mage calls forth a writhing torrent of ephemera, raw Essence, and half-formed spirits, which tears into her foes with gleeful abandon. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts lethal damage. This spell can target physical beings or spirits in Twilight.
\reach[1]{For one point of Mana, the spell inflicts aggravated damage.}
\reach[1]{The mage may divide the spell's Potency between damage rating and destroying Essence contained in the target's Pattern. 1 Potency destroys 1 Essence.}
\reach[1]{The target becomes more vulnerable to spiritual predation: she gains the Open condition.}
\reach[1]{The spell can target beings on the other side of the Gauntlet.}
}

\spell{Shape Spirit}
{Patterning}
{Potency}
{}
{Rank}
{When no tool is ready to hand, the shaman shapes one from what is available. This spell allows the mage to reshape a spirit's fundamental nature. She may invoke a number of the following effects equal to the spell's Potency:
\begin{smallitemize}
\item \textit{Change the spirit's fundamental nature; for example, making a mouse spirit into a spirit of bad luck and mischief.}
\item \textit{Redistribute the spirit's Attribute dots.}
\item \textit{Heal one box of lethal damage from the spirit's Corpus.}
\item \textit{Redefine and redistribute the spirit's Influences.}
\item \textit{Add, remove, or replace one Manifestation.}
\item \textit{Add, remove, or replace one Numen.}
\item \textit{Rewrite the spirit's Ban and Bane.}
\end{smallitemize}
She can also alter the spirit's size, shape, and appearance as she sees fit, within the limits of the spell's Scale factor. The spirit's new traits must stay within its Rank-derived maximums. When the spell's Duration expires, the spirit returns to its original form and capabilities.
\reach[1]{For one Mana, the spell may heal aggravated damage.}
}

\spell{World Walker}
{Patterning}
{Potency}
{}
{Gauntlet Strength}
{The shaman goes where she must to find wisdom and power. This spell allows the mage to bring a subject directly across the Gauntlet, either to or from the Shadow, without the need for a portal. If the subject is a spirit or ephemeral object, it appears in Twilight.
\reach[1]{The mage may grant a conjured spirit or ephemeral object the Materialized Condition, which lasts until the spell's Duration expires.}
}

\spell{Annihilate Spirit}
{Unmaking}
{Potency}
{}
{Rank}
{The most fearsome spell in most shamans' arsenal, this terrible magic utterly destroys a spirit. The target spirit may spend an Essence to roll Power + Finesse in a Clash of Wills, a last ditch attempt to reassert its existence through its Influences. If the spell is successfully cast, the spirit is instantly and utterly destroyed — even if it still has Essence, it does not retreat into hibernation, it is simply gone. Short of archmastery, this spell cannot affect spirits of Rank 6 or higher.
}

\spell{Birth Spirit}
{Making}
{Duration}
{}
{}
{By means of this spell, the mage may coax dormant Essence into life, awakening it as a Rank 1 Spirit. This spirit is not under the mage's particular control, but most newborn spirits feel a kind of respect or gratitude toward their maker. Many mages then use Bolster Spirit and Shape Spirit to improve their ephemeral creation's capabilities.
\reach[1]{For one Mana, the Spirit created is Rank 2.}
}

\spell{Create Locus}
{Making}
{Duration}
{}
{Gauntlet Strength}
{By means of this spell, the mage creates a Locus at a location with the Resonant Condition. A Locus is a location in which the Shadow world is especially close. Spirits don't need the Reaching Manifestation Effect to use their powers across the Gauntlet at a Locus, attempts to cross over are at +2 dice, and spirits whose natures match the Locus' Resonant Condition heal at twice the normal rate.
\reach[1]{The Locus generates a number of Essence per day equal to the spell's Potency.}
}

\spell{Essence Fountain}
{Making}
{Potency}
{}
{}
{The shaman feeds her spiritual children. This spell generates a quantity of Essence equal to the spell's Potency within the subject's Pattern. The Essence has a Resonance of the mage's choosing, as long as she's encountered it before.
\reach[1]{The mage may “flavor” the Essence with multiple Resonances she has previously encountered.}
}

\spell{Spirit Manse}
{Making}
{Duration}
{}
{}
{The master lives among her shadow brethren in a palace of her will. This spell carves out an extradimensional space in the Shadow, one of the fabled “Places That Aren't” that doesn't map to any location in the physical world. The Spirit Manse can take any form the mage desires, but its appearance is heavily colored by her Path and her Nimbus. As long as the spell's Duration lasts, the mage gains the Safe Place Merit at a rating equal to the spell's Potency.
\reach[1]{The mage may create an Iris between her Manse and the material world, allowing anyone to enter the Shadow directly into her Manse. She may craft a Key to this door if she wishes. The spell becomes Withstood by the Gauntlet Strength.}
}

\end{document}