\documentclass{ArcanumSpells}

\begin{document}
\scriptsize
\setArcanum{Prime}

\section{Prime}
\textbf{Purview:} Magic, the Supernal World, Nimbus, truth, Yantras, Mana, Hallows, tass, resonance, revelation.\\
Just as a language must have words to describe itself, so too must the Supernal have an Arcanum that defines it. Prime, the subtle Arcanum that rules the Aether, is that Arcanum. Its purview is the manipulation of magic itself: Mana and tass, the Nimbus and Hallows, High Speech and the runes of the ancient masters. Through Prime, a mage becomes attuned to the Supernal Truth, capable of piercing illusions and calling forth perfected images of the symbol-beings of the Aether. Arrogant Obrimos sometimes claim that this makes Prime the greatest of the Arcana, but that is an oversimplification. Prime is the Arcanum through which the Supernal knows itself, but without the other Arcana, it is as empty as a language whose only vocabulary is parts of speech.

\spell{Dispel Magic}
{Compelling}
{Potency}
{}
{Arcanum rating of the subject spell's caster}
{All Awakened magic contains the capacity to end, to allow the Fallen World's laws to reassert themselves. By Compelling these flaws in an extant spell, the mage may temporarily suppress it - or even destroy it entirely. This spell is not potent enough to dispel an archmage's spells, and only works against Awakened magic. In addition, the mage must include all Arcana involved in the casting of the subject spell at one dot. A successful casting suppresses the spell for the Duration of Dispel Magic.
\conjunction{Add Fate}{1}{The mage may suppress the subject spell selectively, for a number of subjects equal to Dispel Magic's Scale factor.}
\reach[2]{For one point of Mana, the effect is Lasting. If the spell's original caster is still alive and has not relinquished the spell, she knows one of her spells was destroyed.}
}

\spell{Pierce Deception}
{Unveiling}
{Duration}
{}
{}
{Prime is the Arcanum of pure Truth, and no falsehood may stand before it. By means of this spell, the subject sees illusions, phantasms, and lies for what they are. The spell sees through mundane falsehoods the subject perceives automatically; magical illusion or deception automatically provokes a Clash of Wills. This spell only reveals "active" untruths: the subject would see that
someone with dyed hair isn't really a blonde, or recognize a lie when she heard it, but she wouldn't know that a Wall Street executive has been committing tax fraud for years just by looking at him. If she got a look at his tax return, however, she would see that it was a falsehood.
\reach[1]{In addition to sensing falsehoods, the subject gets some symbolic sense of what the actual truth is, veiled in Supernal symbolism and metaphor.}
}

\spell{Supernal Vision}
{Unveiling}
{Potency}
{}
{}
{By opening her subject's third eye, the mage reveals her fellows as the wells of Supernal power they are. By studying a person, place, or location for one turn, the subject automatically knows whether it is connected to the Supernal (e.g. if a person is a mage, a Sleepwalker, a Proximus, or a Sleeper; if a place is a Demesne or Verge; if an object is Imbued, Enhanced, or an Artifact), and may ask a number of the following questions equal to the spell's Potency:
\begin{smallitemize}
\item  \textit{How much Mana does the target have in her Pattern?}
\item  \textit{To which Supernal World is the target most closely aligned?}
\item  \textit{What is the target's highest-rated Arcanum?}*
\item  \textit{How adept is the target at their highest-rated Arcanum?}
\item  \textit{How many Arcana does the subject know?}
\item  \textit{What is the target's Nimbus?}
\item  \textit{What is the target's Gnosis?}
\end{smallitemize}
* Subsequent asking of this question reveals the target's second, third, etc. highest Arcana.\\
The subject perceives the answers as Supernal symbols and visions unfolding around the target. If the subject desires more information about a particular phenomenon, she may study it for multiple turns, as long as the spell's Duration lasts. Effects that would cloak the target's nature provoke a Clash of Wills as normal.
\reach[1]{The spell reveals the nature of supernatural beings and effects other than Supernal phenomena - though there's no guarantee the mage knows what the aura of, for example, a Promethean looks like, and most of the questions will return nonsensical imagery that best translates as "not applicable."}
}

\spell{Sacred Geometry}
{Unveiling}
{Duration}
{}
{}
{While her senses are open to this spell, the subject can clearly perceive ley lines and nodes. Depending on the caster's Path and Nimbus, she might see them as beams of golden light meeting at shining Platonic solids, electric-blue rivers pooling into lakes, or strains of music building into a mighty symphony. If there are no ley lines or nodes within sensory range, the subject feels a tugging sensation toward the nearest ley or node.
\reach[1]{The mage's perceptions expand to tell her when a node sits atop a Hallow.}
\conjunction{Add Death or Spirit}{1}{The mage's senses are also attuned to the presence of Avernian Gates or Loci, respectively. Other Arcana may reveal stranger sacred spaces, as well.}
}

\spell{Scribe Grimoire}
{Compelling}
{Potency}
{1 Mana}
{Total Arcanum dots of all Arcana used in the spell being scribed.}
{By means of this spell, the mage gives physical form to the mudras of a particular Rote, creating a Grimoire (see p. 101). This spell has two slightly different, albeit related, applications: The mage may either inscribe a Rote she knows, or she may copy a Rote from another Grimoire she has on hand. Only a single Rote can be inscribed per casting of this spell, but a given Grimoire may hold multiple Rotes at a time: A large book can hold anywhere from 10-15 Rotes, while a fist-sized carved stone might only hold one or two, and a computer database could hold a theoretically unlimited number. When the spell's Duration expires, the inscribed Rotes fade and cannot be recovered.
\reach[1]{For one point of Mana, the spell's Duration is Lasting.}
}

\spell{Word of Command}
{Compelling}
{Potency}
{Variable}
{}
{Enchanted objects and long-lasting spells often have specific triggers or conditions that must be fulfilled before they will release their magic. With this spell, a mage can bypass those conditions, freeing the magic to do that which it would. The object or spell immediately activates, exactly as though it were triggered by whatever normally triggers the effect. If an activation roll is normally required, treat the spell's Potency as rolled successes. If the subject spell requires Mana to activate, the mage must spend it from her own pool. Without additional Arcana, this spell can only trigger Supernal spells and objects tied to the Supernal World such as Artifacts and Imbued Items.
\conjunction{Add Any Other Arcanum}{1}{By adding the relevant Arcanum, a mage can activate magical effects and objects created by other sources of power - Spirit to activate a fetish, Fate to trigger a faerie's curse, and so on. If this object requires mystical energy (Essence or stranger substances) to activate, the mage may spend Mana in lieu of the normal power source.}
}

\spell{As Above, So Below}
{Ruling}
{Duration}
{}
{}
{A mage's tools are sacred, her every word and deed a reflection of the Realms Above. By means of this spell, the mage imbues the tools of her Art with holy meaning, allowing her to draw down power with greater facility. For every level of Potency, she chooses a single Yantra (and it must be a specific example of a Yantra, not just a category: "the Crypt of the Mariner," not just
"Environment"). Any spell cast that incorporates that Yantra gains the 9-Again effect on the spellcasting roll. 
\reach[1]{The Yantra instead grants the 8-Again effect on spellcasting rolls.}
}

\spell{Cloak Nimbus}
{Veiling}
{Duration}
{}
{}
{This spell cloaks the subject's Nimbus from spells and effects that would read it, such as Supernal Vision or the ability of certain psychics to read emotional states in auras. Any such effect is subject to a Clash of Wills (see p. 117). Any effect that fails to pierce the veil registers the subject as an ordinary Sleeper. Spells cast while under the influence of this spell do not cause the caster's Immediate Nimbus (see p. 89) to flare unless she chooses to. Furthermore, while this spell is active the subject's Signature Nimbus (see p. 89) is muted; any attempt to scrutinize it with Mage Sight provokes a Clash of Wills. If the scrutinizing mage fails, he is unable to find any identifiable traits in the Signature Nimbus. If the subject takes any action that causes her Nimbus to flare, such as allowing it to do so when casting a spell or imprinting her Signature Nimbus on an object, this spell ends immediately.
\reach[1]{Rather than veiling her subject's abilities completely, the mage may project the Nimbus of a lesser magician. For each Reach she applies, she may choose one of Gnosis, Mana, or any Arcanum she knows, and specify a value below her subject's actual Trait rating. Any effect that fails to penetrate her veil registers the false Trait value. The mage cannot pretend to powers the subject does not have: An Adept of Prime can be masked to seem a mere Initiate, but cannot project the Nimbus of a Master, nor can she be made to seem an Apprentice of Space if she has no dots in that Arcanum.}
}

\spell{Invisible Runes}
{Ruling}
{Duration}
{}
{}
{The Awakened make use of signs others can't see. This spell draws a short message in High Speech, visible only to Mage Sight, onto its subject. Attempting to alter the marks by overwriting them provokes a Clash of Wills. Mages use these signs to mark their cabal's property and territory, or leave warnings for one another, as any form of Active Mage Sight reveals them.
}

\spell{Supernal Veil}
{Veiling}
{Duration}
{}
{}
{Sometimes even the greatest magician must hide her light under a bushel. This spell wards its subject, which can be a spell, object, mage, supernatural creature, or any other active magical phenomenon, from detection. Passive abilities (such as Peripheral Mage Sight) automatically fail to detect the veiled phenomenon, while active attempts provoke a Clash of Wills.}

\spell{Wards and Signs}
{Shielding}
{Duration}
{}
{}
{By cloaking her subject in Aetheric symbols of victory and indomitability, the mage shields the subject from the effects of hostile magic. When the subject is the target of a spell, that spell is Withstood with the Potency of Wards and Signs. Only spells that directly target the subject can be Withstood; a spell that turns the air around her into fire cannot be Withstood. Likewise, if the subject is one of many subjects, Wards and Signs only Withstands the spell with regards to her. Other subjects suffer the full effects.
}

\spell{Words of Truth}
{Ruling}
{Potency}
{}
{Composure}
{The mage speaks with tongues of fire, and the world listens. So long as the words the mage speaks are objectively true and the mage herself knows them to be true, all subjects of this spell can hear her and understand her clearly, regardless of distance, noise, or language barriers. Moreover, all subjects know, on a soul-deep level, that what the mage says is true. The spell only works on statements the mage knows to be true: She can't use it to confirm or reject theories. It also doesn't necessarily compel the targets to act on the information in any particular way, but ignoring or refuting this Supernal truth may be grounds for a breaking point. In a Social maneuvering action, this spell may remove one Door or improve the impression level by one step per point of Potency.
\reach[1]{The mage's words don't merely ring with truth, they call to action. If a listener goes along with what the mage said, he gains the Inspired Condition. If, however, he ignores the mage's words, he gains the Guilty Condition.}
}

\spell{Aetheric Winds}
{Weaving}
{Potency}
{}
{}
{The mage calls forth a bare fraction of the howling fury of the Aether, scouring her subject with shrieking winds. This is an attack spell, inflicting bashing damage equal to Potency.
\reach[1]{The winds of this spell stick around after casting, creating a Heavy Winds Environmental Tilt in the immediate vicinity.}
\reach[1]{In lieu of damage, the mage may assign Potency to instead destroy the target's Mana. One level of Potency so assigned destroys one point of Mana, and Potency may be freely split between Mana destruction and damage.}
}

\spell{Channel Mana}
{Weaving}
{Potency}
{}
{Composure (or Rank for Supernal entities)}
{The flows of Supernal energy are the mage's to manipulate. This spell allows the mage to move a quantity of Mana equal to the spell's Potency between one or more vessels she can touch, including other mages, herself, Hallows, Artifacts, and others. She must, however, respect her Gnosis-derived Mana per turn limit.
\reach[1]{The mage may ignore her Mana per turn limit, channeling as much Mana as she desires as an instant action.}
}

\spell{Cleanse Pattern}
{Fraying}
{Potency}
{}
{}
{The Forms making up a subject's Supernal Pattern are marked by the touch of magic. With this spell, a mage removes the tell-tale signs of Awakened interference. The spell removes the dramatic failure effect of a Focused Mage Sight Revelation (p. 92) from a subject. If the spell's subject bears a mage's Signature Nimbus, the spell removes it.}

\spell{Display of Power}
{Weaving}
{Duration}
{}
{}
{Magic itself falls under the purview of Prime, even its most private functions. By using this spell, a mage stirs the Supernal World, making it respond to mages within the spell's area. Rather than being wholly internal, Imagos formed by mages within the spell's effect become visible in the Supernal World to all forms of Active Mage Sight, displayed as magical runes and flashes of symbols hovering around the mage. Mages use this spell as a teaching aid, forming Imagos to display to their students without actually casting. The spell has another role in mage society, though; it is the basis for the Duel Arcane (see p. 294), in which two rival mages display what they could do to one another.
\reach[2]{For one Mana, any attempt to Counterspell (p. 192) a spell cast within the area gains the rote action quality, as the plainly visible Imago makes the spell easy to decipher. (In many duels, seconds are appointed to stand ready to Counterspell attempts at cheating.)}
}

\spell{Ephemeral Enchantment}
{Weaving}
{Duration}
{}
{}
{The symbol-forms of the Aether are real enough to cut through all layers of reality. This spell enchants the subject to be as solid to Twilight entities as to physical matter. This spell is equally effective against all forms of Twilight; the subject may interact with ghosts, spirits, angels, and stranger things with equal facility. 
\reach[2]{If the enchanted object is a weapon, it acts as a bane of the specified entity.}
}

\spell{Geomancy}
{Weaving}
{Duration}
{}
{}
{By imposing her will upon the earth's natural flow of energy, the mage may redirect ley lines within the area of effect, reshaping Nodes and altering Resonance freely. She may move ley lines, and therefore the Nodes created where ley lines cross, "pinning" a line to a point within the area of effect of the spell. She may also change the Resonance Keyword of a Node to whatever she wishes.
}

\spell{Platonic Form}
{Weaving}
{Potency}
{1+ Mana}
{}
{The mage may Pattern Mana into behaving as it does within tass, creating a magical object formed of pure Mana. The object must be a simple object or tool (swords and gemstones are allowable, guns and cars are not.) It is obviously magical, has a default Durability of 1 and consists of one point of Mana (which the mage must pay as part of the casting). Potency may be allocated to the following effects:
\begin{smallitemize}
\item Increase Durability by +1.
\item Increase Mana capacity by +1 (the mage may fill this Mana capacity by spending Mana as part of the casting or leave the object partially empty).
\item If the object can be used as a tool or weapon, add +1 equipment bonus or weapon damage. Each action using the construct as a tool or weapon uses up 1 Mana from its supply. 
\end{smallitemize}
When all Mana is withdrawn from the object, it crumbles to nothing. A mage may "refill" it with the Channel Mana spell or similar effects. When the spell's Duration runs out, any unused Mana sublimates back into the world and is lost.
\conjunction{Add Forces}{3}{The object is not obviously magical.}
\reach[1]{If the object is a tool, it is a perfected ideal of that tool. It grants 8-again.}
\reach[2]{The mana construct can be a complex device.}
}

\spell{Stealing Fire}
{Perfecting}
{Duration}
{}
{Resolve}
{Prometheus brought fire from Olympus to the mortal realm. By means of this spell, the mage brings a tiny fragment of Supernal fire to the sleeping masses, if only for a time. The subject of this spell, who must be a Sleeper, temporarily becomes a Sleepwalker (see p. 303) with all that entails. Any breaking points due to witnessing magic and Quiescence effects the subject would normally suffer are held in abeyance until the spell's Duration expires, only to come crashing down all at once when the spell ends.}

\spell{Apocalypse }
{Patterning}
{Duration}
{}
{Resolve}
{The subject of this spell has the scales of the Lie removed from his eyes. Anyone subject to this spell - mage, Sleeper, or other supernatural being - gains Mage Sight attuned to the Path of the caster. Along with this gift comes temporary immunity to the Quiescence curse. It does not, however, prepare the target for how to interpret the visions received under Mage Sight, and the uninitiated are likely to face breaking points due to the trauma of the Sight. While Awakened subjects may control the new Sight as though it were their own, focusing it and pushing it back to the Periphery like their own, other subjects gain Active Mage Sight and cannot shut the Sight off - it lasts until the spell's Duration expires, but still applies dice pool penalties and Willpower costs as per Mage Sight (see p. 90). If the subject runs out of Willpower points and the spell is still active, he gains the Blind Condition as the Supernal vision burns out his eyes. (At the Storyteller's discretion, this might be replaced with Deafened or a similar Condition if the subject experiences Mage Sight with other senses).
\reach[1]{The mage may add an Arcanum (that she has dots in) to the granted Sight, but must pay Mana to add Common or Inferior Arcana as though she were activating Mage Sight herself.}
}

\spell{Celestial Fire}
{Patterning}
{Potency}
{}
{}
{The mage summons the Supernal fires of the Aether to smite her enemies. This is not base, Fallen flame, but rather the pure expression of Awakened will. This is an attack spell; its damage rating is equal to the spell's Potency, and it inflicts lethal damage. The spell affects Twilight entities.
\reach[1]{The celestial fires of this spell ignite flammable objects in the scene.}
\reach[1]{For one point of Mana, the spell inflicts aggravated damage.}
\reach[1]{In lieu of damage, the mage may assign Potency to instead destroy the target's Mana. One level of Potency so assigned destroys one point of Mana, and Potency may be freely split between Mana destruction and damage.}
}

\spell{Destroy Tass}
{Unraveling}
{Potency}
{}
{Durability}
{At the mage's whim, constructs of Mana are swept away on the winds of Aether. A successful casting destroys the tass. The Mana held within it is not destroyed, but sublimates into the world and likely returns to the nearest Hallow.}

\spell{Eidolon}
{Making}
{Potency}
{1+ Mana}
{}
{Where disciples of Prime can create platonic objects by forcing Mana into the pattern it wears within tass, a master can create the complex Prime patterns within living beings and the environment. When fuelled with Mana, this spell creates an obviously-magical construct much like Platonic Form, except that the construct is not limited to single physical objects. Eidolons may imitate fire, fog, and even entire environments, but most masters use it to create "living" constructs. Eidolons are still made of solidified Mana, have Durability and Structure instead of Health, and when mimicking environmental hazards do not inflict damage as they do (Eidolon fire does not burn, for example.) They follow the Potency rules for Platonic Form (p.169) but Potency may also be allocated to grant dots of the Retainer Merit. Though animate, the construct is mindless. If used as a Retainer, the Eidolon's "field" includes simple physical labor, combat, and other uncomplicated tasks. The Eidolon can only execute whatever order the caster gave it last. Orders must be very simple. If attacked, the Eidolon has no Defense. Unlike Platonic forms, Eidolons do not crumble when all Mana is withdrawn. A mage may refill the Eidolon as per Platonic Form. When the spell's Duration runs out, any unused mana sublimates back into the world and is lost.
\conjunction{Add Forces}{3}{The construct is not obviously magical.}
\conjunction{Add Mind}{4}{ The construct may be given a mind of its own. See p. 165 for details.}
}

\spell{Hallow Dance}
{Patterning}
{Potency}
{}
{Hallow Rating}
{The tides of Aether ebb and flow, awakening sacred places and sending them to sleep once more in the cycle of ages. This spell allows the mage to bend that cycle to her will. The mage may suppress an active Hallow or awaken a dormant one with this spell. Rousing a slumbering Hallow requires a Potency equal to the Hallow's rating, while damping a Hallow reduces its effective dot rating by one per point of Potency. If the Hallow is suppressed to zero dots or fewer, it falls dormant. See p. 241 for more information on Hallows.
\reach[2]{For one point of Mana, the effect is Lasting.}
}

\spell{Supernal Dispellation}
{Unraveling}
{Potency}
{}
{Arcanum rating of the subject spell's caster}
{Supernal truths can never truly be unmade, but with this spell the mage may cast them back across the Abyss, effectively erasing any spell she comes across. This spell is not potent enough to dispel archmages' spells. A successful casting suppresses the spell for the Duration of Supernal Dispellation.
\conjunction{Add Fate}{1}{The mage may suppress the subject spell selectively, for a number of subjects equal to the Dispel's Scale factor.}
\reach[2]{For one point of Mana, the effect is Lasting. If the spell's original caster is still alive and has not relinquished the spell, she knows one of her spells was destroyed.}
}

\spell{Blasphemy }
{Unmaking}
{Potency}
{}
{Hallow Rating, if applicable}
{By defining all truths, the Supernal includes the means of its own erasure. This spell severs the world's connection with the Supernal, creating a "dead zone" in which the energies of life simply cease to be. The spell has the following effects:
\begin{smallitemize}
\item Ley lines within the area dry up and die. Nodes similarly cease to function.
\item Hallows whose rating is less than the spell's Potency fall dormant.
\item Sleepers who spend more than a day within the area gain the Enervated condition (though this is not soul loss and the victims do not progress to the Thrall Condition, see p. 315).
\item Any attempt to reawaken a Hallow within the area adds the Potency of this spell to the Hallow's dot rating for purposes of Withstanding the effect.
\end{smallitemize}\fixEmptyNewline{}
\reach[2]{The effects are Lasting, though without major geomantic workings to maintain the barren state, within a month or so natural rhythms will reassert themselves and ley lines, nodes, and the health of the Sleepers will return. Hallows, however, remain dormant.}
}

\spell{Create Truth}
{Making}
{Duration}
{5 Mana per Potency}
{Hallow Rating of desired Hallow}
{The Awakened speaks, and the heavens reshape themselves. This spell overwrites the conditions of Fallen Reality within the area, creating a Hallow with a dot rating equal to the spell's Potency. This Hallow has Resonance appropriate to its location and to the caster's Path and Nimbus (see p. 242). The sudden emergence of such a mystically potent site causes massive ripples and aftershocks through the local network of ley lines, which almost certainly creates new Mysteries. Hallows "cap out" at a rating of $\spelldots{5}$; any Potency in excess has no effect.
\reach[2]{For 5 Mana, the effect becomes Lasting.}
}

\spell{Forge Purpose}
{Making}
{Duration}
{}
{Resolve}
{The mage imparts a holy mission upon her subject. For the Duration of the spell, the subject gains one of the mage's Obsessions as his own. If the subject is a mage who already has the maximum number of Obsessions allowed by her Gnosis, this spell triggers a Clash of Wills. If the caster is successful, she replaces the subject's most recently acquired Obsession with her own. Even Sleepers, who normally only have Aspirations, gain an Obsession. Though they can't generally spend Arcane Experiences, they still accrue them and may spend the Experiences if they ever Awaken. Some mages believe Sleepers who have experienced the Mysteries are more likely to Awaken - empirical evidence, however, suggests that mental illness is more likely.
\reach[1]{The mage may create a wholly new Obsession rather than copy one of her own.}
}

\spell{Word of Unmaking}
{Unmaking}
{Potency}
{}
{Merit rating of the targeted magical object, or Durability if not measured in Merit dots}
{The Aetheric storms scour and destroy as much as they revitalize. With this spell, the mage calls down the destructive power of the Supernal to destroy a magical item. Supernal Artifacts cannot be destroyed by this spell.
\reach[2]{The item explodes violently when destroyed. Roll the object's Merit rating (or Durability) as a dice pool: Anyone within 1 yard per dot suffers a point of lethal damage per success.}
}
\end{document}