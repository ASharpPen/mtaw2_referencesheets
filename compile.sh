#!/bin/bash

outdir=./Bin/
topDir=$PWD

#Load custom packages and classes
echo -------------------------------------------
echo Registering TEXINPUTS directories
echo -------------------------------------------

while IFS="" read -r p || [ -n "$p" ]
do
	echo $p
	export TEXINPUTS=$p;
done < ./Tex/TexInputs.txt

#Search for tex files in all folders
echo -------------------------------------------
echo Compiling tex files
echo -------------------------------------------

/usr/bin/find . -name "*.tex" -print0 | while read -d $'\0' fname; do

	#Extract folder structure of file, and ensure output directory exists.
	out=${fname#*/*/}
	out=$outdir${out%/*.*}
	if [[ ! -d $out ]]; then
		mkdir -p $out
	fi
	f=${fname##*/}
	f=${f%.tex}
	
	#Compile file
	latexmk -pdf -jobname="$out/$f" -interaction=nonstopmode $fname
	
	#Clean up build files.
	cd $out/
	rm -f *.aux *.bbl *.blg *.brf *.dvi *.fls *.log *.out *.ps *.toc *.fdb_latexmk
	cd $topDir
done
